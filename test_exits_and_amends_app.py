#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 31 12:46:43 2021

@author: saurav
"""

from kanhoji_external_signals.meru_app import ZMQLocalClient, CONSTANTS
import time
import logging
import datetime
import numpy as np
import random

logger = logging.getLogger(__name__)

class MyApp(object):
    def __init__(self, ema1=10, ema2=20):
        """
        Regular initialization of your trading app. Say it has two init params
        decided by you:
            ema1  and ema2 
            
        All we need to to do is add an attr in your app called `zmq_client`
        as shown below in last line of this init code.

        Parameters
        ----------
        ema1 : int, optional
            DESCRIPTION. The default is 10.
        ema2 : int, optional
            DESCRIPTION. The default is 20.

        Returns
        -------
        None.

        """
        self.ema1 = ema1
        self.ema2 = ema2
        
        # We only need you to add this attr `self.zmq_client` along with port 
        # and strategy_id! The port should be one of the ports specified in 
        # item `PORT_LOCAL` inside the Meru app config's file: `meru_configuration.py`. 
        # Each stategy will have its unique port and this port won't be shared 
        # with any other strategy.

        # Whenever you need to place an order call the method.
        # `self.zmq_client.meru_place_order`
        self.zmq_client = ZMQLocalClient(port=5557, strategy_id='EATCPB1')


    @property
    def meru_messages_today(self):
        """
        Show all the messages sent to Meru servers today in form of pandas
        dataframe. It can be filtered for extract specific signals.

        Returns
        -------
        pd.DataFrame
            index = message_id.
            columns = ['signaltype', 'timestamp', 'tag', 'product', 'trigger_price', 'price',
                       'quantity', 'strategy_id', 'action', 'ticker', 'message_id',
                       'ordertype']
        Example
        -------
        >>> self.messages_today_as_dataframe
        Out[1]: 
           signaltype         timestamp                     tag product  \
        1   NEW_ENTRY  1632033893476397  Hi9G5hkVEeybB2Vm20dsWg     MIS   
        2  STOP_ENTRY  1632033893476406  Hi9G5hkVEeybB2Vm20dsWg     MIS   
        
           trigger_price  price  quantity strategy_id action ticker  message_id  \
        1              0      0         1       ALGO1    BUY    ACC           1   
        2              0      0         1       ALGO1    BUY    ACC           2   
        
                ordertype  
        1  SingleLegOrder  
        2  SingleLegOrder
        """        
        return self.zmq_client.messages_today_as_dataframe
    
    def meru_place_single_leg_order(self, 
            ticker='ACC', 
            action='BUY',
            tag=None,
            strategy_id=None,
            quantity=0,
            price=0,
            trigger_price=0,
            product=CONSTANTS.PRODUCT_MIS,            
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            signaltype=CONSTANTS.SIGNALTYPE_NEW_ENTRY,
            ):
        """
        Demostration of placing CONSTANTS.ORDERTYPE_SINGLELEGORDER to 
        Merus's server!
        
        params are explained in method `meru_place_single_leg_order` of the 
        `ZMQLocalClient` instance in `self.zmq_client` attr.

        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments

        action : {'BUY', 'SELL'}
            'BUY' or 'SELL'       
            
        tag : str/int of max length 36 chars. (optional)
            
            For NEW_ENTRY:
            A tag (say order_id) of your choice for the order being 
            placed. It should originate from your trading application.
            It is a free field assigned to you for sending your custom
            field. Ideally it should be unique.

            36 characters allows uuid to be passed as tag.
            
            The default in None, in which case:
            For NEW_ENTRY:
                it will be autmatically set to auto-generated uuid which is 
                converted to base64 str.
                * This auto-generated string can contain special characters*
        
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.
                    
        quantity : int, optional
            The actual quantity to be bought or sold.
            If it is a derivative ticker, still provide number of shares and 
            not number of lots! For example, for trading two lots on BANKNIFTY 
            futures where each lot is of qty 25, send `quantity` param as 50.
        
            The default is 0, which is provided for only for testing purposes,
            i.e. testing communication between Meru and your app.
            
        price : float, optional
            The `price` param for order. 
            The default is 0.
            
        trigger_price : float, optional
            The `trigger_price` param for order. 
            The default is 0.
            
            if price==0 and trigger_price==0: means MARKET order
            if price>0 and trigger_price==0: means LIMIT order
            if price==0 and trigger_price>0: means SL_M order
            if price>0 and trigger_price>0: means SL order
            
            The order_type is not being sent separately as it can be deduced
            from logic above.
                    
        product : {'MIS','NRML'}, optional
            Product type for the order. 
            The default is `MIS`.

        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
        signaltype : {CONSTANTS.SIGNALTYPE_NEW_ENTRY,}
            signaltype to be used. It shd match the value of one of 
            specified values.        

            Only one signaltype allowed. 
            The variable is left for future development.
                                    
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` 
        and thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.

        """
        # A signal  genrated using your custom logic
        return self.zmq_client.meru_place_single_leg_order(ticker=ticker, action=action, 
            tag=tag, strategy_id=strategy_id, quantity=quantity, price=price, 
            trigger_price=trigger_price, product=product, 
            timestamp=timestamp, signaltype=signaltype)
    
    def meru_stop_single_leg_order(self, 
            ticker='RELIANCE', 
            tag=None,
            strategy_id=None,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            signaltype=CONSTANTS.SIGNALTYPE_STOP_ENTRY,
            ):
        """
        Demostration of stopping CONSTANTS.ORDERTYPE_SINGLELEGORDER to 
        Merus's server!
        
        params are explained in method `meru_stop_single_leg_order` of the 
        `ZMQLocalClient` instance in `self.zmq_client` attr.
        
        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments
            It will be ignored. It is to help downstream apps, 

        tag : str/int of max length 36 chars.
            
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.
             
        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
        signaltype : {CONSTANTS.SIGNALTYPE_NEW_ENTRY,}
            signaltype to be used. It shd match the value of one of 
            specified values.        

            Only one signaltype allowed. 
            The variable is left for future development.
                                    
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` 
        and thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.
                
        Note2
        -----
        Some fields are going to be auto-appended to the message when it is
        being sent by the RabbitMQ client like `message_id`, `dealer_id`.
    
        Incidently, (`message_id`, `dealer_id`) are reserved keywords and 
        don't use them in in this class.
            
            `message_id`: A auto incrementing unqiue key for each message
                sent by you. This is internal to and maintained by this 
                'ZMQLocalClient` class and you don't have to do anything.
                
                It will help in reconciliation at both your end and ours.

            `dealer_id` : dealer_id assigned to you by us. It will help us 
                indentify which partner sent us this message as multiple partners
                might be running their algos with us.                .

        """
        # A signal  genrated using your custom logic
        return self.zmq_client.meru_stop_single_leg_order(ticker=ticker, 
            tag=tag, strategy_id=strategy_id, signaltype=signaltype,
            timestamp=timestamp)    
    
    def meru_place_bracket_order(self, 
            ticker='RELIANCE', 
            action='SELL', 
            tag=None,
            strategy_id=None,
            quantity=0,
            price=88.0,
            trigger_price=89.0,
            product=CONSTANTS.PRODUCT_MIS,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            signaltype=CONSTANTS.SIGNALTYPE_NEW_ENTRY,
            stoploss_price=113.2,
            stoploss_trigger_price=112.2,
            takeprofit_price=22.5
            ):
        """
        Demostration of placing CONSTANTS.ORDERTYPE_BRACKETORDER to 
        Merus's server!
        
        params are explained in method `meru_place_bracket_order` of the 
        `ZMQLocalClient` instance in `self.zmq_client` attr.

        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments

        action : {'BUY', 'SELL'}
            'BUY' or 'SELL'        
            
        tag : str/int of max length 36 chars.
            
            For NEW_ENTRY:
            A tag (say order_id) of your choice for the order being 
            placed. It should originate from your trading application.
            It is a free field assigned to you for sending your custom
            field. Ideally it should be unique.

            36 characters allows uuid to be passed as tag.
            
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.
                    
        quantity : int, optional
            The actual quantity to be bought or sold.
            If it is a derivative ticker, still provide number of shares and 
            not number of lots! For example, for trading two lots on BANKNIFTY 
            futures where each lot is of qty 25, send `quantity` param as 50.
        
            The default is 0, which is provided for only for testing purposes,
            i.e. testing communication between Meru and your app.
            
        price : float, optional
            The `price` param for order. 
            The default is 0.
            
        trigger_price : float, optional
            The `trigger_price` param for order. 
            The default is 0.
            
            if price==0 and trigger_price==0: means MARKET order
            if price>0 and trigger_price==0: means LIMIT order
            if price==0 and trigger_price>0: means SL_M order
            if price>0 and trigger_price>0: means SL order
            
            The order_type is not being sent separately as it can be deduced
            from logic above.
                    
        product : {'MIS','NRML'}, optional
            Product type for the order. 
            The default is `MIS`.
            

            
        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
        signaltype : {CONSTANTS.SIGNALTYPE_NEW_ENTRY,}
            signaltype to be used. It shd match the value of one of 
            specified values.        

            Only one signaltype allowed. 
            The variable is left for future development.
            

        stoploss_price : float, optional
            The `stoploss_price` param for order. 
            The default is 0.
            It is used in EXIT_SL leg.
            
        stoploss_trigger_price : float, optional
            The `stoploss_trigger_price` param for order. 
            The default is 0.
            It is used in EXIT_SL leg.
            
            if stoploss_price==0 and stoploss_trigger_price==0: means MARKET order
                This value is ignored as it implied exit immediately when Entry
                is recieved.
            if stoploss_price>0 and stoploss_trigger_price==0: means LIMIT order
                This is also ignored as SL order can be either SL_M or SL order. 
            if stoploss_price==0 and stoploss_trigger_price>0: means SL_M order
            if stoploss_price>0 and stoploss_trigger_price>0: means SL order
            
            The order_type is not being sent separately as it can be deduced
            from logic above.
                                    
        takeprofit_price : float, optional
            The `takeprofit_price` param for order. 
            The default is 0.
            It is used in EXIT_TP leg.
            If 0 is provided, there is no takeprofit_target and is ignored.
                        
        Note
        ----
        This method pushes a CONSTANTS.ORDERTYPE_BRACKETORDER to Meru.
        CONSTANTS.ORDERTYPE_BRACKETORDER: When you want to place
            a Signal which has both entry and exit legs built-in.
            
            Entry leg: provided input params `price` and `trigger_price` 
                indicate the prices for entry leg. They also automatically 
                set the order_type ('MARKET', 'LIMIT', 'SL-M', 'SL'). So 
                order_type is not needed seperately.

            EXIT SL leg: provided input params `stoploss_price` and 
                `stoploss_trigger_price` indicate the prices for exit at SL 
                leg. They also automatically set the order_type ('MARKET', 
                'LIMIT', 'SL-M', 'SL'). So order_type is not needed seperately.
                
            EXIT TP leg: provided input params `takeprofit_price` indicate 
                the price for exit at TP leg. It is always a 'LIMIT' order.
                
            When entry happens:
                a. both the EXIT_SL and EXIT_TP orders (if levels provided) 
                    will be placed at exchange automatically 
                b. if either one of  them is hit, the other will be cancelled 
                    automatically. It is One Cancel Other (OCO) order.
            
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` and
        thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.

        """
        # A signal  genrated using your custom logic
        return self.zmq_client.meru_place_bracket_order(ticker=ticker, action=action, 
            tag=tag, strategy_id=strategy_id, quantity=quantity, price=price, 
            trigger_price=trigger_price, product=product, 
            timestamp=timestamp, signaltype=signaltype,
            stoploss_price=stoploss_price,
            stoploss_trigger_price=stoploss_trigger_price,
            takeprofit_price=takeprofit_price
            )
        
    
    def meru_stop_bracket_order(self, 
            ticker='RELIANCE', 
            tag=None,
            strategy_id=None,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            signaltype=CONSTANTS.SIGNALTYPE_STOP_ENTRY,
            ):
        """
        Demostration of stopping CONSTANTS.ORDERTYPE_SINGLELEGORDER to 
        Merus's server!
        
        params are explained in method `meru_stop_single_leg_order` of the 
        `ZMQLocalClient` instance in `self.zmq_client` attr.
        
        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments
            It will be ignored. It is to help downstream apps, 

        tag : str/int of max length 36 chars.
            
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.
             
        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
        signaltype : {CONSTANTS.SIGNALTYPE_NEW_ENTRY,}
            signaltype to be used. It shd match the value of one of 
            specified values.        

            Only one signaltype allowed. 
            The variable is left for future development.
                                    
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` 
        and thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.
                
        Note2
        -----
        Some fields are going to be auto-appended to the message when it is
        being sent by the RabbitMQ client like `message_id`, `dealer_id`.
    
        Incidently, (`message_id`, `dealer_id`) are reserved keywords and 
        don't use them in in this class.
            
            `message_id`: A auto incrementing unqiue key for each message
                sent by you. This is internal to and maintained by this 
                'ZMQLocalClient` class and you don't have to do anything.
                
                It will help in reconciliation at both your end and ours.

            `dealer_id` : dealer_id assigned to you by us. It will help us 
                indentify which partner sent us this message as multiple partners
                might be running their algos with us.                .

        """
        # A signal  genrated using your custom logic
        return self.zmq_client.meru_stop_bracket_order(ticker=ticker, 
            tag=tag, strategy_id=strategy_id, signaltype=signaltype,
            timestamp=timestamp)
    
    def meru_exit_bracket_order(self,
            ticker='RELIANCE', 
            tag=None,
            strategy_id=None,
            signaltype=CONSTANTS.SIGNALTYPE_EXIT_MARKET,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            ):
        """        
        Demostration of placing CONSTANTS.ORDERTYPE_BRACKETORDER to 
        Merus's server!
        
        params are explained in method `meru_exit_bracket_order` of the 
        `ZMQLocalClient` instance in `self.zmq_client` attr.
        
        Parameters
        ----------     
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments

        tag : str/int of max length 36 chars.
        
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.     
            
        signaltype : CONSTANTS.SIGNALTYPE_EXIT_MARKET
         
            signaltype to be used. It shd match the value of one of three
            specified values.        
            
            If you want to exit the Signal/Position at Current Market price,
            use CONSTANTS.SIGNALTYPE_EXIT_MARKET.
                        
        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
            
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` and
        thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.
        
        """
        # A signal  genrated using your custom logic
        return self.zmq_client.meru_exit_bracket_order(ticker=ticker, 
            tag=tag, strategy_id=strategy_id, signaltype=signaltype,
            timestamp=timestamp)
    
    def meru_get_tag_for_amending_signal(self, ticker, action, quantity=None, 
                            price=None, trigger_price=None, product=None):
        """
        Extract the tag during placing a `NEW_ENTRY` signal for given
        params. This is required to be provided for cancelling/amending orders
        as it is use by Meru to isolate the signal/order which requires 
        modifications.
        
        If you are maintaining this tag at your end, there is no need to use
        this method. But if you didn't provided a tag during `NEW_ENTRY` signal
        then `self.zmq_client` automatically provides a unique uuid tag (compressed 
        to base64 str).
        
        The `self.zmq_client` contains a property called `messages_today_as_dataframe`
        and can be accessed via `self.zmq_client.messages_today_as_dataframe`.
        It is a pandas dataframe with details of all signals sent to Meru via
        the Meru App. This dataframe is filtered to extract the signal's tag(s).
                
        If a param is not provided, or None is proved, it will
        be ignored while querying the tag.
        
        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments

        action : {'BUY', 'SELL'}
            'BUY' or 'SELL'            
                    
        quantity : float, optional
            The actual quantity to be bought or sold.
            If it is a derivative ticker, still provide number of shares and 
            not number of lots! For example, for trading two lots on BANKNIFTY 
            futures where each lot is of qty 25, send `quantity` param as 50.
        
            The default is 0, which is provided for only for testing purposes,
            i.e. testing communication between Meru and your app.
            
        price : float, optional
            The `price` param for order. 
            The default is 0.
            
        trigger_price : float, optional
            The `trigger_price` param for order. 
            The default is 0.
            
            if price==0 and trigger_price==0: means MARKET order
            if trigger_price>0 and trigger_price==0: means LIMIT order
            if price==0 and trigger_price>0: means SL_M order
            if price>0 and trigger_price>0: means SL order
            
            The order_type is not being sent separately as it can be deduced
            from logic above.
                    
        product : {'MIS','NRML'}, optional
            Product type for the order. 
            The default is `MIS`.

        Returns
        -------
        list of tags which match the filters. Even if only one item is there,
        it will still return a list.
        
        An empty list will be returned in case there is no signal found or
        error is encountered.

        Example
        ------
        >>> self.meru_get_tag_for_amending_signal(ticker='ACC', action='BUY', quantity=3)
        Out[10]: ['Hi9G5hkVEeybB2Vm20dsWg']
        """
        return self.zmq_client.get_tag_for_signal(ticker=ticker, action=action, 
            quantity=quantity, price=price, trigger_price=trigger_price, 
            product=product)
    
    def meru_place_order_loop(self, till_time=datetime.time(23,35)):
        """
        Run an endless loop for sending messages. Useful for testing purposes.

        Parameters
        ----------
        till_time : datetime.time, optional
            DESCRIPTION. The default is datetime.time(23,35).

        Returns
        -------
        None.

        """
        tag = 1111
        while True:
            logger.info("Placing Order...")
            tag += 1
            message_id1 = self.meru_place_single_leg_order(tag=tag)
            logger.info("Success! Recieved SingleLegOrder message_id: %s "
                        "for tag..%s"%(message_id1, tag))
            tag += random.randint(1,2)
            message_id2 = self.meru_place_bracket_order(tag=tag)       
            logger.info("Success! Recieved BracketOrder Entry message_id: %s "
                        "for tag..%s"%(message_id2, tag))
            if tag % 2 == 0:
                message_id3 = self.meru_exit_bracket_order(tag=tag)
                logger.info("Success! Recieved BracketOrder Exit message_id: %s "
                            "for tag..%s"%(message_id3, tag))            
            else:
                message_id3 = self.meru_stop_bracket_order(tag=tag)
                logger.info("Success! Recieved BracketOrder STOP ENTRY message_id: %s "
                            "for tag..%s"%(message_id3, tag))            
                
            if datetime.datetime.now().time() > till_time:
                break
            time.sleep(np.random.randint(0,10))
        
#%%
if __name__ == "__main__":
    # Initialize logging
    import logging
    formatter = "%(asctime)s.%(msecs)03d | %(module)s | %(levelname)s | %(lineno)d | %(message)s"
    DATE_FORMAT = '%H:%M:%S'    
    logging.basicConfig(format=formatter, datefmt=DATE_FORMAT)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)   
    
    myapp = MyApp()
    
    ticker = 'BANKNIFTY2443049300CE'
    action = 'SELL'
    action2 = 'BUY'
    quantity = 15
    stoploss_price1 = 250.0
    stoploss_trigger_price1 = 251.

    stoploss_price2 = 260.
    stoploss_trigger_price2 = 261.
    
    tag = 11111
    
# =============================================================================
    #%% 1. Place SingleLegOrder
    myapp.meru_place_single_leg_order(ticker=ticker, action=action, quantity=quantity*2, tag=tag)
    
    #%% 2. Stop SingleLegOrder
    myapp.meru_stop_single_leg_order(ticker=ticker, tag=tag)
    
    #%% 3. Place BracketOrder
    tag += 1
    myapp.meru_place_bracket_order(ticker=ticker, action=action2, 
                quantity=quantity*2, tag=tag, 
                trigger_price=stoploss_trigger_price2)
        
    #%% 4. Place the exit order for BracketOrder
    myapp.meru_exit_bracket_order(ticker=ticker, tag=tag,
                            signaltype=CONSTANTS.SIGNALTYPE_EXIT_MARKET)
    #%% 5. Place BracketOrder
    tag += 1
    myapp.meru_place_bracket_order(ticker=ticker, action=action, 
                quantity=quantity*2, tag=tag, 
                price=121.2, trigger_price=120.2,
                stoploss_price=stoploss_price1, 
                stoploss_trigger_price=stoploss_trigger_price1,
                takeprofit_price=55.0)
        
    #%% 6. Stop the Entry for BracketOrder
    myapp.meru_stop_bracket_order(ticker=ticker, tag=tag,
                            signaltype=CONSTANTS.SIGNALTYPE_STOP_ENTRY)
    
    
    #%% 7. Find tag:
    tag = myapp.meru_get_tag_for_amending_signal(ticker=ticker, action=action2, 
                    quantity=quantity*2, trigger_price=stoploss_trigger_price2)[-1]
    #%% 8. Run loop:
    myapp.meru_place_order_loop(till_time=datetime.time(17,57))
