#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 13 10:31:39 2021

@author: saurav
"""

9. v6a dated 2024-04-29
------------------------
BREAKING CHANGE

a. Make two ordertypes: 
        SingleLegOrder: only a Buy or sell signal with no exit leg 
        BracketOrder: A signal with entry and exit legs defined.
            It has three legs:
                entry: (Marker, SL, SLM and LIMIT)
                exit_SL: (SLM or SL)
                exit_TP: (LIMIT)
            Once entry order is filled the exit_SL and exit_TP legs will be 
            placed in exchange automatically. When either of exit_SL and exit_TP 
            will be filled, the other will be cancelled automatically.
            
b. Add methods in the meru_app.ZMQLocalClient to 
    a. place new SingleLegOrder order.
    b. Stop an existing SingleLegOrder order.
    c. Place new BracketOrder order.
    b. Stop an existing BracketOrder order.
    c. Exit at Market an existing BracketOrder order when entry is filled.
    
c. The users using meru_app have to provide `tag` field (some sort of order_id
    from their system) for all Signals, except the Place new Entry signal in
    SingleLegOrder where it will be optional and then can get system_generated. 
    
d. In the next development, create specialized method for each activity like
    place order, modify order etc rather than have a super method handling all.
    
    
8. v5a dated 2021-12-14
------------------------
FULLY BACKWARD COMPATIBLE.

a. Add threading.RLock in meru_app.ZMQLocalClient app.
    When the meru_app.ZMQLocalClient is called multiple times with a delay,
    the next signal can be placed by the app before the 'message_id' can
    be incremented. The Lock will prevent that.
b. Tested this error using ThreadPoolExecutor. Original solution without a lock
    was raising an Error described above. Addition of lock got rid of that 
    error. Negligible latencies (<1ms) introduced in process.

    
7. v4a dated 2021-09-19
------------------------
FULLY BACKWARD COMPATIBLE.

a. Add functionality to support Amending/Cancelling Existing Signals.
b. Amending/Cancelling Existing Signals requires tag input to be passed, the same 
    tag which was used while placing the signal entry. Further, such tags should 
    be a unique for the day.
c. MeruApp can automatically generate this tag, if user wants Meru to manage it.
    He has to provide tag=None while placing a NEW_ENTRY. This auto-generated
    tag may contain special characters.
d. A helper method has been added to extract this tag by filtering signal
    parameters called `meru_app.ZMQLocalClient.get_tpbasag_for_signal`.
e. The only change for user is that a new param `signaltype` has been added
    in meru_app.ZMQLocalClient.meru_place_order which is used to pass information
    on whether it is a NEW_ENTRY, or AMEND_ENTRY or STOP_ENTRY signal.
    Existing user can ignore it and default value is good for them.
f. A new attr meru_app.ZMQLocalClient.messages_today_as_dataframe has been
    added which displays all messages sent today to Meru in pandas dataframe.

6. v3a dated 2021-08-04
-----------------------
a. Add functionality to support sending signals for multiple strategies.
b. To do this, replace existing PAIR scoket with PUBSUB socket. 
c. Each individual strategy is a publisher, publishing its orders and bound its
    independent port.
d. The ZMQ client inside the `MeruApp` as a subscriber and listen to all the 
    messages sent by each of the strategies and republishes to Meru Servers.    

5. v2c dated 2021-05-19
-----------------------
a. Solved Bug: replaced MARKET_END_TIME with CONSTANT.MARKET_END_TIME in meru_app.py.
    Because of this bug, the connection was not restarting as the code
    was not able to resolve MARKET_END_TIME.

4. v2b dated 2021-02-18
-----------------------
a. create a new module constants.py to hold all constant values.
b. added 'strategy_id' attr to meru.app.ZMQLocalClient.
    This strategy_id can be added automatically to the message if user doesnot
    provide this param in meru_place_order call.
c. Auto-append 'ordertype', 'signaltye' and 'app_version' to the messages.
    User can ignore these.
d. Store Last sent messages as att 'message' in rabbitmq_publisher.
e. Add a batch file `windows_batch_file_start_MeruApp.bat` which is sample batch file
    to run this app on windows. User has to create a local copy with right
    path. This batch file can be used to launch the app on windows.
f. Add a windows scheduled task 'Windows_scheduled_task_to_start_goMeruApp.xml'
    which can be secduled to start the MeruApp in the morning.

3. v2a dated 2021-02-13 
-----------------------
a. Recconecting feature stabilized.
b. Significant work around maintaining and resending pending messagaes.
c. documentation updated.

2. v1b dated 2021-02-13 
-----------------------
a. Updated name of rabbitmq_publisher.py module to meru_app.py module.
b. This will need a change in the client trading applications as they need to 
    change the import path for `ZMQLocalClient` client used by them.
c. Introduced the param LOG_PATH in configuration file where meru app's log file     
    will be placed, else it will be placed in Home directory. Client can
    set this LOG_PATH in configuration to set thier own logging folder.
d. Updated documentation.
e. in `MeruApp` wait for rabbitmq publisher to connect to server before recieving
    messages from local client.
f. All tag field in a message to upto 36 characters so that uuid can be passed.


1. v1a dated 2021-02-13 
-----------------------
Initial version.