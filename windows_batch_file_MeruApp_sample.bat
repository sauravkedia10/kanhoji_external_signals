Rem Activate the anaconda environment by putting the right path and calling its activate.bat
call C:\Users\Administrator\anaconda3\Scripts\activate.bat

Rem Navigate to folder where MeruApp is installed.
cd C:\Users\Administrator\anaconda3\lib\site-packages\kanhoji_external_signals\

Rem Run with python in this anaconda environment, don't use ipython. Ipython hangs after some time.
python -i meru_app.py