# README #

This repository is targeted at our partners who wish to share signals with
us.

### What is this repository for? ###

* Shares a python app `MeruApp` which will send trading signal from our
partner's (i.e your) trading application to our server over RabbitMQ.

* This standalone app `MeruApp` will run independently in a separate process in
your(partner's) machine.

* You will have to add a lightweight component `ZMQLocalClient` in your
app which will send the trading signals to this `MeruApp` over a `ZMQ` port.

* This `MeruApp` will receive these trading signals over `ZMQ` port and forward
to Meru Servers over a `RabbitMq` connection.

* The architecture is such that all heavvy lifting of sending messages to Meru
servers is done by `MeruApp`. It will not interfere with the your trading 
application and can simply be killed if it is causing any issues.
 
* `MeruApp` operates in Fire and Forget mode. You will get a confirmation
that signal has been recieved by Meru servers or not, but after that you will
not get any update/response/order_id. You will have to track subsequent activity 
on Meru's website `mountmerucapital.com`.
 
### How do I get set up? ###

* Ideally, Meru will set up the Meru App for you.

* Cross Platform: works on all platforms!

* Dependencies: 
    1. Core ZMQ app should installed on the your machine. Refer ZMQ official website.
    2. No need to install core rabbitmq and Erlang. Rabbitmq is required only
        for communication between Meru Servers and `MeruApp` and the Rabbitmq 
        server for that purpose is hosted inside Meru servers. At your
        end Rabbitmq python library `pika` will suffice.
    3. Some python libraries like `pika` and `pyzmq` needs to be installed.
    
    `pip install pyzmq==18.0.0 pika==1.1.0`
    
### I want to set it up myself, help please? ###

* Clone this repo. Clone it, don't just download zip and unzip it. We will keep
pushing updates regularly. We will try very hard that our new versions are 
backward compatible and don't impact your already running instances.

* Create a copy of the module `meru_configuration_sample.py` within this repo
and rename it `meru_configuration.py`. It is your configuration file and will
contain configuration data pertaining to your installation. Don't use 
`meru_configuration_sample.py` for you configuration as it is a part of the 
repo and will be overwritten everytime we push an update.

* refer to file `zmq_inside_your_app.py`. It gives a demo of how `MeruApp` can
be added to your strategies. Say your strategy code is written in a class called
`zmq_inside_your_app.MyApp`. Then you need to create an attribute called 
`MyApp.zmq_client = ZMQLocalClient(port=5556, strategy_id='ALGO1')` in your
app.

* from the provided module `zmq_inside_your_app.MyApp`, you can add the methods 
`meru_messages_today`, `meru_place_order`, `meru_amend_order` and 
`meru_get_tag_for_amending_signal` in your strategy code, or you may use them 
directly from `MyApp.zmq_client` attribute.

* `MeruApp` supports requirement that you are able to send signals for multiple 
strategies each running in thier own independent and separate CPU process at your
end. All you need to do is to add the attribute `zmq_client` to each of the strategies,
but keep port numbers different (incrementing starting from 5556) for each strategy. 
A single instance of `MeruApp` will support all these strategies, so it needs to be 
started only once in the morning.

* The `ticker` param for placing/amending/cancelling signals will be as per
`ZERODHA` notation.

* There is a common method `MyApp.zmq_client.meru_place_order` to place new signal
and amend/cancel exisiting signals. For placing new signals, set `signaltype` param
as `CONSTANTS.SIGNALTYPE_NEW_ENTRY`; for modifying, set `signaltype` param
as `CONSTANTS.SIGNALTYPE_AMEND_ENTRY`; and for cancelling/stopping set `signaltype` param
as `CONSTANTS.SIGNALTYPE_STOP_ENTRY`.
 
* While amending signals, only price and trigger_price can be amended. Sometime it
may lead to conversion of ordertypes from one of these to other `Market|Limit|SL-M|SLL`.
This is fine and will happen automatically. However, quantity or action (`BUY|SELL`)
can't be changed. To do that pass two requests, first to cancel the existing signal
and then to place a new signal with required params.
 
* There is `tag` input while placing/amending/cancelling signals. This is a free
character field (upto 36 characters) for passing your custom string to identify 
this signal (similar to `tag` input provided by ZERODHA). You may or maynot
provided it while placing a new signal. If you don't provide it, `MeruApp` will 
generate a unique UUID tag automatically.

* This `tag` is needed while amending/cancelling signal. If you need to amend/cancel
signals and are passing `tag` and it needs to be unqiue for the day for a 
given strategy. If your are not passing `tag`, then `MeruApp` will generate 
it automatically use UUID. In this case, you can use the method 
`MyApp.zmq_client.get_tag_for_signal` to find it while amending or cancelling 
the signal. A wrapper method `meru_get_tag_for_amending_signal` is included in 
`zmq_inside_your_app.py` for demo. You can also use the attribute 
`MyApp.zmq_client.messages_today_as_dataframe` to filter it out.

*  In the morning, at around 8.35 or so, the `MeruApp` should be started though 
cron job. For Linux, it is pretty straightforward: use `crontab`. For Windows 
necessary guidance is provided below.

* `MeruApp` will generate logs as it receives signals from your strategies,
sends them to Meru servers and receives confirmations back from Meru servers.
They are stored in your `$HOME` (`~` i.e `/home/user` in Linux and `C:\Users\user\`
in Windows) as `MeruApp_2021-09-09.log` wherein the date part wil change each
day.

### Scheduling it as a Task on Windows ###

* If you are a Windows user, then we have included utilities: 
    * a batch scipt to start `MeruApp` and 
    
    * a Task file which can imported into the Windows Task Scheduler to schedule 
    the `MeruApp` to start at 8.45 am every morning. This Task will run the
    batch file above every morning at 8.45 am.

* To uses the Windows utilities:
    * create copies of these two files:
        - `windows_batch_file_MeruApp_sample.bat` and rename to 
        `windows_batch_file_MeruApp.bat`
        - `windows_scheduled_task_MeruApp_sample.xml` and rename to 
        `windows_scheduled_task_MeruApp.xml`

    * Look at all file paths in both the copied files and check if they are 
    right for your machine. Currently they are setup up for user Administrator 
    on Windows Server 2016 instance.
    
    * Double click on the `windows_batch_file_MeruApp.bat` and check if it is
    working fine. If this is a regular weekday between 8.30 am to 10.30 p.m.
    it should connect to Meru, and print in console that it has connected to
    Meru' Servers with following lines.
    
        08:16:09.506 | meru_app | INFO | 792 | Rabbitmq Channel connected with Meru Servers for user: ...!        
        08:16:10.522 | meru_app | INFO | 1427 | Started the Meru ZMQ Client!
        
    If Meru Servers are switched off, the console should still open and it should 
    log some connections errros, similar to:
    
        09:27:13.360 | meru_app | ERROR | 729 | EALTPL Rabbitmq connection open failed, reopening in 5 seconds:
        Traceback (most recent call last):
          File "C:\Users\Administrator\anaconda3\lib\site-packages\pika\adapters\utils\connection_workflow.py", line 815, in _try_next_resolved_address
            addr_record = next(self._addrinfo_iter)
        StopIteration
        
    * To kill the MeruApp, you can simply kill this python console. There is no
    need to shutdown gracefully. 
    
    * Import the task (local copy) `windows_scheduled_task_MeruApp.xml` in the 
    Windows Task scheduler. A new `Create Task` window will pop up. Do the following:
        
        * In the `General` Tab, check `Security Options`
        
            * Select `Run only when user is logged on`
        
            * Above it current User (`Administrator` (in my Windows 2016 server)) 
            should already by populated, if not select correct User from 
            `Change User or Group..` button.
        
            * In `Trigger` tab select the right time. Plase specify as per the 
            time zone of your server. We want the task to start by `0845 IST`
        
            * If server is time zone UTC then specifiy `0315 UTC`.
        
            * In `Actions` tab :
                * `Action` should be : `Start a program`
                * `Details`: full path of `windows_batch_file_MeruApp.bat` i.e. 
                the local copy of the batch file created earlier.
        
            * Press Ok.
        
            * Task should be setup and you should see a new tak in Task library:
                `windows_scheduled_task_MeruApp`
        
            * In the Task Scheduler App, select this Task, right click and then
            select `Run` and check whether it is working or not! If it works,
            it will open a python console and print the two log lines confirming 
            that `MeruApp` has connected as mentioned earlier. It requires that
            Meru Servers are on at that time. If it shows connection related 
            errors, then most likely Meru Servers are switched off at that time.
        
            * If working, close the Meru App by killing the console. Try and 
            change the Time Trigger in this task to a time after 2 Minutes and 
            test again if it launches on its own.
        
            * If successful, we are done. Change the time trigger back to 
            `0845 IST`.
        
            * The next day please verify that it indeeed launched at `0845 IST`.
            
    * To kill the `MeruApp` at day end, simply kill this python console running
    the MeruApp. There is no need to shutdown gracefully.
            
    * If you are longer sending the signals to Meru, you can simply delete this
    task from the Task Scheduler as well as the repo and remove the `ZMQLocalClient`
    from your trading application.

**PLEASE CREATE LOCAL COPIES OF THERE TWO WINDOWS FILES. THE SAMPLE FILES ARE
PART OF THIS REPO AND RISK BEING OVERWRITTEN IN LATER UPDATES!**
            
### ChangeLog ###

7. v4a dated 2021-09-19
------------------------
FULLY BACKWARD COMPATIBLE.

    1. Add functionality to support Amending/Cancelling Existing Signals.
    2. Amending/Cancelling Existing Signals requires tag input to be passed, the same 
        tag which was used while placing the signal entry. Further, such tags should 
        be a unique for the day.
    3. MeruApp can automatically generate this tag, if user wants Meru to manage it.
        He has to provide tag=None while placing a NEW_ENTRY. This auto-generated
        tag may contain special characters.
    4. A helper method has been added to extract this tag by filtering signal
        parameters called `meru_app.ZMQLocalClient.get_tpbasag_for_signal`.
    5. The only change for user is that a new param `signaltype` has been added
        in meru_app.ZMQLocalClient.meru_place_order which is used to pass information
        on whether it is a NEW_ENTRY, or AMEND_ENTRY or STOP_ENTRY signal.
        Existing user can ignore it and default value is good for them.
    6. A new attr meru_app.ZMQLocalClient.messages_today_as_dataframe has been
        added which displays all messages sent today to Meru in pandas dataframe.

6. v3a dated 2021-08-04
-----------------------
NOT BACKWARD COMPATIBLE.

    1. Add functionality to support sending signals for multiple strategies.
    2. To do this, replace existing PAIR scoket with PUBSUB socket. 
    3. Each individual strategy is a publisher, publishing its orders and bound its
        independent port.
    4. The ZMQ client inside the `MeruApp` as a subscriber and listen to all the 
        messages sent by each of the strategies and republishes to Meru Servers.    


5. v2c dated 2021-05-19
-----------------------
FULLY BACKWARD COMPATIBLE.

    1. Solved Bug: replaced MARKET_END_TIME with CONSTANT.MARKET_END_TIME in meru_app.py.
        Because of this bug, the connection was not restarting as the code
        was not able to resolve MARKET_END_TIME.


4. v2b dated 2021-02-18
-----------------------
FULLY BACKWARD COMPATIBLE.

    1. create a new module constants.py to hold all constant values.
    2. added 'strategy_id' attr to meru.app.ZMQLocalClient. Client can pass this
        param to the `ZMQLocalClient` isntance withing his trading applications. Once
        done, this strategy_id can be added automatically to the message if user doesnot
        provide this param in `meru_place_order` call.
    3. Auto-append 'ordertype', 'signaltye' and 'app_version' to the messages.
        User can ignore these.
    4. Store Last sent messages as attr 'message' in rabbitmq_publisher (for testing etc).
    5. Add a batch file `windows_batch_file_MeruApp_sample.bat` which is sample batch file
        to run this app on windows. User has to create a local copy with right
        path. This batch file can be used to launch the app on windows.
    6. Add a sample Windows Scheduled Task `windows_scheduled_task_MeruApp_sample.xml`.
        User has to create a local copy with right path. This task can be imported in
        Windows Task Scheduler to lauch the MeruApp automatically in the morning.

3. v2a dated 2021-02-13 
-----------------------
    1. Recconecting feature stabilized.
    2. Significant work around maintaining and resending pending messagaes.
    3. documentation updated.

2. v1b dated 2021-02-13 
-----------------------
    1. Updated name of rabbitmq_publisher.py module to meru_app.py module.
    2. This will need a change in the client trading applications as they need to 
        change the import path for `ZMQLocalClient` client used by them.
    3. Introduced the param LOG_PATH in configuration file where meru app's log file     
        will be placed, else it will be placed in Home directory. Client can
        set this LOG_PATH in configuration to set thier own logging folder.
    4. Updated documentation.
    5. in `MeruApp` wait for rabbitmq publisher to connect to server before recieving
        messages from local client.
    6. All tag field in a message to upto 36 characters so that uuid can be passed.


1. v1a dated 2021-02-13 
-----------------------
    1. Initial version. 
    
### Contribution guidelines ###

### Who do I talk to? ###

* Saurav Kedia: Co-founder of Meru Captials at saurav.kedia@merucapitals.com
