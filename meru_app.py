
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 12:23:55 2019

@author: saurav

NOTES
-----
1. Authoritative AMQP documenatation
https://www.rabbitmq.com/amqp-0-9-1-reference.html#domain.no-wait

2. MESSAGE IS SIMPLY AN ORDER SENT BY YOU! In programming term we 
    call it a message!

3. For the documentation, you means our algo partner who is sending us 
    the signals and us/we/our means Meru Capitals!
    
4. We are sending this you as a proprietory code for handling our 
    interactions. Please don't distribute it onwards without prior permission 
    from Meru Capitals!
    
"""

import functools
import logging
import json
import pika
import zmq
import time
import threading
import datetime
import os
import base64
import uuid
import pandas as pd

# create a file called `meru_configuration.py` which is based on 
# `meru_configuration_sample.py` in this repo folder.
# File `meru_configuration.py` will be added to .gitignore and won't be
# commited

from kanhoji_external_signals.meru_configuration import (PORT_LOCAL, HOST, PORT, 
    VIRTUAL_HOST, USER, DEALER_ID, APP_ID, ROUTING_KEY, NAME, PASSWORD, EXCHANGE_NAME, 
)


try:
    from kanhoji_external_signals.meru_configuration import LOG_PATH
except:
    LOG_PATH = None

import  kanhoji_external_signals.constants as CONSTANTS

logger = logging.getLogger(__name__)


class ZMQLocalClient(object):
    """
    Local lightweight super reliable ZMQ client called `local_client` 
    which will be attached to your main trading application.
    
    It will have a corresponding paired listner which will be housed in a separate
    app `MeruApp` which will communicate with Meru (our) servers. That listner 
    will listen to message/order send by this `local_client` attached to your 
    trading app and forward to our servers.
    
    We have not added a automatic reconnect feature to this local client as it 
    is very stable. Automatic reconnect feature is added to `MeruApp`.
    
    In case you are running multiple strategies, create an independent instance 
    of this class inside each of your strategy code, as an attr of that startegy. 
    While initializing it inside the strategy, it is necessary to provide 
    explicitly the port and strategy_id. Don't pass None. 
    
    So, an independent isntance of this class will be attached to each strategy.
    
    """
    def __init__(self, port, strategy_id):
        """
        Initialize the local_client.

        Parameters
        ----------
        port : int
            the port to which `local_client` will send messages to! 
            The port should be one the ports specified in the item `PORT_LOCAL`
            in the `meru_configuration.py`.
            
        strategy_id : str
            the strategy_id to be seeded to the orders being sent to Meru servers.
            It has to be whitelisted at Meru servers and has to be prvided by
            Meru Capitals.
            
        Note
        ----
        Each strategy will have its unique port and strategy_id and either of
        these values (port or strategy_id) shouldn't be repeated in any other 
        strategy.
        
        Further, the port should be one the ports specified in the item 
        `PORT_LOCAL` in the `meru_configuration.py`.

        Returns
        -------
        None.

        """
        self.message_id = 1
        """int
        
        A auto incrementing message_id which will be seeded to each message
        sent by you. The purpose is to assign a unique key to each message.
        
        It will help in reconciliation at both your end and ours.
        
        The message_id is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` 
        and thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.
        """
        
        self.strategy_id = strategy_id[:16]
        """str (maz 16 characters)
        
        The strategy_id of the strategy to which this client will be attached.
        
        Recall that if you are sending signals for multiple strategies, then
        each strategy will have its own instance of the `ZMQLocalClient` while
        only one instance of `MeruApp` will run and handle all stategies.
        
        The `strategy_id` provided here can be used to auto-populate the 
        strategy_id field of the message.
        
        The `strategy_id` has be be provided by Meru Capitals as there is 
        whitelisting of stategy_id's at its ends.
        
        """
        
        self.messages_today = dict()
        """
        dict of dict.
        The dictionary of messages which has been sent today.
            key : `message_id`
            value: all fields in message.

        """
        
        if port is not None:
            message = "Meru App: the specified port %s is not included in item `PORT_LOCAL` in Meru App's config file: `meru_configuration.py`! Please add it and restart."%port
            if isinstance (PORT_LOCAL, tuple):
                assert port in PORT_LOCAL, message
            else:
                assert port == int(PORT_LOCAL), message

        else:                
            port = PORT_LOCAL[0] if isinstance (PORT_LOCAL, tuple) else PORT_LOCAL
        
        self.port = port        
        
        self.lock = threading.RLock()
        """
        theading.RLock
        Lock used to block the method `meru_place_order` while placing the
        order. Else if two signals are generated together, say while placing
        order for a straddle, then the two signals can fire this method and
        place signal over `zmq` before the `message_id` can be incremented.
        So, both the orders will have same `message_id`.
        
        The `message_id` is used later as a key in the `MeruApp` and if it is
        repeated it leads to error in the delivery acknowledgment in
        `RabbitmqReconnectingPublisher.on_delivery_confirmation`. 
        
        Despite this, while the message is delivered properly to Meru servers, 
        it corrupts the logs at client's side and creates avoidable confusion.
        
        """
        context = zmq.Context()
        self.socket = context.socket(zmq.PUB)
        # self.socket.bind("tcp://localhost:%s" % port)
        self.socket.bind("tcp://*:%s" % port)
        
        time.sleep(2)
        
    @property
    def messages_today_as_dataframe(self):
        """
        Show all the messages sent to Meru servers today in form of pandas
        dataframe. It can be filtered for extract specific signals.

        Returns
        -------
        pd.DataFrame
            index = message_id.
            columns = ['signaltype', 'timestamp', 'tag', 'product', 'trigger_price', 'price',
                       'quantity', 'strategy_id', 'action', 'ticker', 'message_id',
                       'ordertype']
        Example
        -------
        >>> self.messages_today_as_dataframe
        Out[1]: 
           signaltype         timestamp                     tag product  \
        1   NEW_ENTRY  1632033893476397  Hi9G5hkVEeybB2Vm20dsWg     MIS   
        2  STOP_ENTRY  1632033893476406  Hi9G5hkVEeybB2Vm20dsWg     MIS   
        
           trigger_price  price  quantity strategy_id action ticker  message_id  \
        1              0      0         1       ALGO1    BUY    ACC           1   
        2              0      0         1       ALGO1    BUY    ACC           2   
        
                ordertype  
        1  SingleLegOrder  
        2  SingleLegOrder
        """
        return pd.DataFrame.from_dict(self.messages_today, orient='index')
    
    def meru_place_single_leg_order(self, ticker, action,
            tag=None,
            strategy_id=None,
            quantity=0,
            price=0,
            trigger_price=0,
            product=CONSTANTS.PRODUCT_MIS,            
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            signaltype=CONSTANTS.SIGNALTYPE_NEW_ENTRY,
            ):
        """
        Method to send New Entry order message to the Meru servers for a 
        CONSTANTS.ORDERTYPE_SINGLELEGORDER order.
        
        This message/order will be sent to this app running `RabbitmqReconnectingPublisher` 
        class  which  will listen to these messages/orders and in turn forward them 
        to Meru Servers!

        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments

        action : {'BUY', 'SELL'}
            'BUY' or 'SELL'       
            
        tag : str/int of max length 36 chars. (optional)
            
            For NEW_ENTRY:
            A tag (say order_id) of your choice for the order being 
            placed. It should originate from your trading application.
            It is a free field assigned to you for sending your custom
            field. Ideally it should be unique.

            36 characters allows uuid to be passed as tag.
            
            The default in None, in which case:
            For NEW_ENTRY:
                it will be autmatically set to auto-generated uuid which is 
                converted to base64 str.
                * This auto-generated string can contain special characters*
        
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.
                    
        quantity : int, optional
            The actual quantity to be bought or sold.
            If it is a derivative ticker, still provide number of shares and 
            not number of lots! For example, for trading two lots on BANKNIFTY 
            futures where each lot is of qty 25, send `quantity` param as 50.
        
            The default is 0, which is provided for only for testing purposes,
            i.e. testing communication between Meru and your app.
            
        price : float, optional
            The `price` param for order. 
            The default is 0.
            
        trigger_price : float, optional
            The `trigger_price` param for order. 
            The default is 0.
            
            if price==0 and trigger_price==0: means MARKET order
            if price>0 and trigger_price==0: means LIMIT order
            if price==0 and trigger_price>0: means SL_M order
            if price>0 and trigger_price>0: means SL order
            
            The order_type is not being sent separately as it can be deduced
            from logic above.
                    
        product : {'MIS','NRML'}, optional
            Product type for the order. 
            The default is `MIS`.

        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
        signaltype : {CONSTANTS.SIGNALTYPE_NEW_ENTRY,}
            signaltype to be used. It shd match the value of one of 
            specified values.        

            Only one signaltype allowed. 
            The variable is left for future development.
                                    
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` 
        and thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.
                
        Note2
        -----
        Some fields are going to be auto-appended to the message when it is
        being sent by the RabbitMQ client like `message_id`, `dealer_id`.
    
        Incidently, (`message_id`, `dealer_id`) are reserved keywords and 
        don't use them in in this class.
            
            `message_id`: A auto incrementing unqiue key for each message
                sent by you. This is internal to and maintained by this 
                'ZMQLocalClient` class and you don't have to do anything.
                
                It will help in reconciliation at both your end and ours.

            `dealer_id` : dealer_id assigned to you by us. It will help us 
                indentify which partner sent us this message as multiple partners
                might be running their algos with us.                .

        """
        
        assert action in ('BUY', 'SELL'), ('param `action` should be '
            'either "BUY" or "SELL". Inputted: %s'%action)
            
        assert isinstance (quantity, int) and (quantity >= 0), ('param `quantity` '                                                                
            'should be a positive integer >=0. Inputted: %s'%quantity)
            
        assert isinstance (price, (int, float)) and (price>= 0), ('param `price` '
            'should be a positive number >=0. Inputted: %s'%price)
                        
        assert isinstance (trigger_price, (int, float)) and (trigger_price>= 0), ('param `trigger_price` '
            'should be a positive number >=0. Inputted: %s'%trigger_price)        
                        
        assert product in (CONSTANTS.PRODUCT_MIS, CONSTANTS.PRODUCT_NRML), (
            'param `product` should be either "MIS" or "NRML". Inputted: %s'%product)    
            
        assert signaltype == CONSTANTS.SIGNALTYPE_NEW_ENTRY, (
            'param `signaltype` should be `%s` only. Inputted: %s'%(
                    CONSTANTS.SIGNALTYPE_NEW_ENTRY , signaltype)
            )

        assert isinstance (timestamp, int) and (timestamp>= 0), ('param `timestamp` '
            'is epoch time and should be a integer >=0. Inputted: %s'%timestamp)
        
        if tag:
            # if tag is a collection like list, tuple, set, the the step
            # to groupby('tag').last() will fail in the `get_tag_for_signal` 
            # method.
            assert isinstance(tag, (int, float, str)), ('Input `tag` can be '
            'None or of type str, int and float! Provided type: %s')%type(tag)

        with self.lock:
            """
            Use a lock so that self.message_id is incremented properly.
            """
            try:
                message = locals()
                del(message['self'])
                if (signaltype==CONSTANTS.SIGNALTYPE_NEW_ENTRY) and (tag is None):          
                    # Send a unique tag using uuid.
                    # convert it to base64 for compresssing it.
                    tag = base64.urlsafe_b64encode(uuid.uuid1().bytes
                                                    ).decode('utf8').rstrip('=\n')        
    
                message['tag'] = tag[:36] if isinstance(tag, str) else tag
    
                message['strategy_id'] = (strategy_id[:16] if strategy_id is not 
                                              None else self.strategy_id)
                message['message_id'] = self.message_id
                message['ordertype'] = CONSTANTS.ORDERTYPE_SINGLELEGORDER
                message['signaltype'] = signaltype
                message['price'] = float(message.get('price', 0))
                message['trigger_price'] = float(message.get('trigger_price', 0))
                
                # add to messages_today dict
                self.messages_today[self.message_id] = message
                self.socket.send_json(message)
                self.message_id += 1
                return message['message_id']
            except:
                logger.exception(message)
                return 0 
            
    def meru_stop_single_leg_order(self, ticker,
            tag,
            strategy_id=None,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            signaltype=CONSTANTS.SIGNALTYPE_STOP_ENTRY,
            ):
        """
        Method to send STOP_ENTRY order message to the Meru servers for a         
        CONSTANTS.ORDERTYPE_SINGLELEGORDER order.
        
        This message/order will be sent to this app running `RabbitmqReconnectingPublisher` 
        class  which  will listen to these messages/orders and in turn forward them 
        to Meru Servers!

        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments
            It will be ignored. It is to help downstream apps, 

        tag : str/int of max length 36 chars.
            
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.
             
        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
        signaltype : {CONSTANTS.SIGNALTYPE_NEW_ENTRY,}
            signaltype to be used. It shd match the value of one of 
            specified values.        

            Only one signaltype allowed. 
            The variable is left for future development.
                                    
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` 
        and thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.
                
        Note2
        -----
        Some fields are going to be auto-appended to the message when it is
        being sent by the RabbitMQ client like `message_id`, `dealer_id`.
    
        Incidently, (`message_id`, `dealer_id`) are reserved keywords and 
        don't use them in in this class.
            
            `message_id`: A auto incrementing unqiue key for each message
                sent by you. This is internal to and maintained by this 
                'ZMQLocalClient` class and you don't have to do anything.
                
                It will help in reconciliation at both your end and ours.

            `dealer_id` : dealer_id assigned to you by us. It will help us 
                indentify which partner sent us this message as multiple partners
                might be running their algos with us.                .

        """
            
        assert signaltype == CONSTANTS.SIGNALTYPE_STOP_ENTRY, (
            'param `signaltype` should be `%s` only. Inputted: %s'%(
                    CONSTANTS.SIGNALTYPE_STOP_ENTRY, signaltype)
            )

        assert isinstance (timestamp, int) and (timestamp>= 0), ('param `timestamp` '
            'is epoch time and should be a integer >=0. Inputted: %s'%timestamp)
        
        assert tag is not None, "`tag` has to be provided for STOP_ENTRY!"        

        with self.lock:
            """
            Use a lock so that self.message_id is incremented properly.
            """
            try:
                message = locals()
                del(message['self'])   
                message['tag'] = tag[:36] if isinstance(tag, str) else tag
    
                message['strategy_id'] = (strategy_id[:16] if strategy_id is not 
                                              None else self.strategy_id)
                message['message_id'] = self.message_id
                message['ordertype'] = CONSTANTS.ORDERTYPE_SINGLELEGORDER
                message['signaltype'] = signaltype
                
                # add to messages_today dict
                self.messages_today[self.message_id] = message
                self.socket.send_json(message)
                self.message_id += 1
                return message['message_id']
            except:
                logger.exception(message)
                return 0             
            
    def meru_place_bracket_order(self, ticker, action, 
            tag,
            strategy_id=None,
            quantity=0,
            price=0,
            trigger_price=0,
            product=CONSTANTS.PRODUCT_MIS,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            signaltype=CONSTANTS.SIGNALTYPE_NEW_ENTRY,
            stoploss_price=0,
            stoploss_trigger_price=0,
            takeprofit_price=0
            ):
        """
        Method to send New Entry order message to the Meru servers for a 
        CONSTANTS.ORDERTYPE_BRACKETORDER order.
        
        This message/order will be sent to this app running `RabbitmqReconnectingPublisher` 
        class  which  will listen to these messages/orders and in turn forward them 
        to Meru Servers!

        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments

        action : {'BUY', 'SELL'}
            'BUY' or 'SELL'        
            
        tag : str/int of max length 36 chars.
            
            For NEW_ENTRY:
            A tag (say order_id) of your choice for the order being 
            placed. It should originate from your trading application.
            It is a free field assigned to you for sending your custom
            field. Ideally it should be unique.

            36 characters allows uuid to be passed as tag.
            
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.
                    
        quantity : int, optional
            The actual quantity to be bought or sold.
            If it is a derivative ticker, still provide number of shares and 
            not number of lots! For example, for trading two lots on BANKNIFTY 
            futures where each lot is of qty 25, send `quantity` param as 50.
        
            The default is 0, which is provided for only for testing purposes,
            i.e. testing communication between Meru and your app.
            
        price : float, optional
            The `price` param for order. 
            The default is 0.
            
        trigger_price : float, optional
            The `trigger_price` param for order. 
            The default is 0.
            
            if price==0 and trigger_price==0: means MARKET order
            if price>0 and trigger_price==0: means LIMIT order
            if price==0 and trigger_price>0: means SL_M order
            if price>0 and trigger_price>0: means SL order
            
            The order_type is not being sent separately as it can be deduced
            from logic above.
                    
        product : {'MIS','NRML'}, optional
            Product type for the order. 
            The default is `MIS`.
            

            
        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
        signaltype : {CONSTANTS.SIGNALTYPE_NEW_ENTRY,}
            signaltype to be used. It shd match the value of one of 
            specified values.        

            Only one signaltype allowed. 
            The variable is left for future development.
            

        stoploss_price : float, optional
            The `stoploss_price` param for order. 
            The default is 0.
            It is used in EXIT_SL leg.
            
        stoploss_trigger_price : float, optional
            The `stoploss_trigger_price` param for order. 
            The default is 0.
            It is used in EXIT_SL leg.
            
            if stoploss_price==0 and stoploss_trigger_price==0: means MARKET order
                This value is ignored as it implied exit immediately when Entry
                is recieved.
            if stoploss_price>0 and stoploss_trigger_price==0: means LIMIT order
                This is also ignored as SL order can be either SL_M or SL order. 
            if stoploss_price==0 and stoploss_trigger_price>0: means SL_M order
            if stoploss_price>0 and stoploss_trigger_price>0: means SL order
            
            The order_type is not being sent separately as it can be deduced
            from logic above.
                                    
        takeprofit_price : float, optional
            The `takeprofit_price` param for order. 
            The default is 0.
            It is used in EXIT_TP leg.
            If 0 is provided, there is no takeprofit_target and is ignored.
                        
        Note
        ----
        This method pushes a CONSTANTS.ORDERTYPE_BRACKETORDER to Meru.
        CONSTANTS.ORDERTYPE_BRACKETORDER: When you want to place
            a Signal which has both entry and exit legs built-in.
            
            Entry leg: provided input params `price` and `trigger_price` 
                indicate the prices for entry leg. They also automatically 
                set the order_type ('MARKET', 'LIMIT', 'SL-M', 'SL'). So 
                order_type is not needed seperately.

            EXIT SL leg: provided input params `stoploss_price` and 
                `stoploss_trigger_price` indicate the prices for exit at SL 
                leg. They also automatically set the order_type ('MARKET', 
                'LIMIT', 'SL-M', 'SL'). So order_type is not needed seperately.
                
            EXIT TP leg: provided input params `takeprofit_price` indicate 
                the price for exit at TP leg. It is always a 'LIMIT' order.
                
            When entry happens:
                a. both the EXIT_SL and EXIT_TP orders (if levels provided) 
                    will be placed at exchange automatically 
                b. if either one of  them is hit, the other will be cancelled 
                    automatically. It is One Cancel Other (OCO) order.
            
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` and
        thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.
                
        Note2
        -----
        Some fields are going to be auto-appended to the message when it is
        being sent by the RabbitMQ client like `message_id`, `dealer_id`.
    
        Incidently, (`message_id`, `dealer_id`) are reserved keywords and 
        don't use them in in this class.
            
            `message_id`: A auto incrementing unqiue key for each message
                sent by you. This is internal to and maintained by this 
                'ZMQLocalClient` class and you don't have to do anything.
                
                It will help in reconciliation at both your end and ours.

            `dealer_id` : dealer_id assigned to you by us. It will help us 
                indentify which partner sent us this message as multiple partners
                might be running their algos with us.                .

        """
        
        assert action in ('BUY', 'SELL'), ('param `action` should be '
            'either "BUY" or "SELL". Inputted: %s'%action)
            
        assert isinstance (quantity, int) and (quantity >= 0), ('param `quantity` '                                                                
            'should be a positive integer >=0. Inputted: %s'%quantity)
            
        assert isinstance (price, (int, float)) and (price>= 0), ('param `price` '
            'should be a positive number >=0. Inputted: %s'%price)
                        
        assert isinstance (trigger_price, (int, float)) and (trigger_price>= 0), ('param `trigger_price` '
            'should be a positive number >=0. Inputted: %s'%trigger_price)
        
        assert isinstance (stoploss_price, (int, float)) and (stoploss_price>= 0), ('param `stoploss_price` '
            'should be a positive number >=0. Inputted: %s'%stoploss_price)
                        
        assert isinstance (stoploss_trigger_price, (int, float)) and (stoploss_trigger_price>= 0), ('param `stoploss_trigger_price` '
            'should be a positive number >=0. Inputted: %s'%stoploss_trigger_price)

        assert isinstance (takeprofit_price, (int, float)) and (takeprofit_price>= 0), ('param `takeprofit_price` '
            'should be a positive number >=0. Inputted: %s'%takeprofit_price)
                        
        assert product in (CONSTANTS.PRODUCT_MIS, CONSTANTS.PRODUCT_NRML), (
            'param `product` should be either "MIS" or "NRML". Inputted: %s'%product)    
            
        assert signaltype == CONSTANTS.SIGNALTYPE_NEW_ENTRY, (
            'param `signaltype` should be `%s` only. Inputted: %s'%(
                    CONSTANTS.SIGNALTYPE_NEW_ENTRY , signaltype)
            )
        
        assert isinstance (timestamp, int) and (timestamp>= 0), ('param `timestamp` '
            'is epoch time and should be a integer >=0. Inputted: %s'%timestamp)

        assert tag is not None, "`tag` has to be provided for CONSTANTS.ORDERTYPE_BRACKETORDER!"
        
        if tag:
            # if tag is a collection like list, tuple, set, the the step
            # to groupby('tag').last() will fail in the `get_tag_for_signal` 
            # method.
            assert isinstance(tag, (int, float, str)), ('Input `tag` can be '
            'None or of type str, int and float! Provided type: %s')%type(tag)


        with self.lock:
            """
            Use a lock so that self.message_id is incremented properly.
            """
            try:
                message = locals()
                del(message['self'])
                message['tag'] = tag[:36] if isinstance(tag, str) else tag
    
                message['strategy_id'] = (strategy_id[:16] if strategy_id is not 
                                              None else self.strategy_id)
                message['message_id'] = self.message_id
                message['ordertype'] = CONSTANTS.ORDERTYPE_BRACKETORDER
                message['signaltype'] = signaltype
                message['price'] = float(message.get('price', 0))
                message['trigger_price'] = float(message.get('trigger_price', 0))
                message['stoploss_price'] = float(message.get('stoploss_price', 0))
                message['stoploss_trigger_price'] = float(message.get('stoploss_trigger_price', 0))
                message['takeprofit_price'] = float(message.get('takeprofit_price', 0))
                
                # add to messages_today dict
                self.messages_today[self.message_id] = message
                self.socket.send_json(message)
                self.message_id += 1
                return message['message_id']
            except:
                logger.exception(message)
                return 0 

    def meru_stop_bracket_order(self, ticker,
            tag,
            strategy_id=None,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            signaltype=CONSTANTS.SIGNALTYPE_STOP_ENTRY,
            ):
        """
        Method to send STOP_ENTRY order message to the Meru servers for a 
        CONSTANTS.ORDERTYPE_SINGLELEGORDER order. Conversly, the entire
        Order is cancelled.
        
        This message/order will be sent to this app running `RabbitmqReconnectingPublisher` 
        class  which  will listen to these messages/orders and in turn forward them 
        to Meru Servers!

        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments
            It will be ignored. It is to help downstream apps, 

        tag : str/int of max length 36 chars.
            
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.
             
        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
        signaltype : {CONSTANTS.SIGNALTYPE_NEW_ENTRY,}
            signaltype to be used. It shd match the value of one of 
            specified values.        

            Only one signaltype allowed. 
            The variable is left for future development.
                                    
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` 
        and thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.
                
        Note2
        -----
        Some fields are going to be auto-appended to the message when it is
        being sent by the RabbitMQ client like `message_id`, `dealer_id`.
    
        Incidently, (`message_id`, `dealer_id`) are reserved keywords and 
        don't use them in in this class.
            
            `message_id`: A auto incrementing unqiue key for each message
                sent by you. This is internal to and maintained by this 
                'ZMQLocalClient` class and you don't have to do anything.
                
                It will help in reconciliation at both your end and ours.

            `dealer_id` : dealer_id assigned to you by us. It will help us 
                indentify which partner sent us this message as multiple partners
                might be running their algos with us.                .

        """
            
        assert signaltype == CONSTANTS.SIGNALTYPE_STOP_ENTRY, (
            'param `signaltype` should be `%s` only. Inputted: %s'%(
                    CONSTANTS.SIGNALTYPE_STOP_ENTRY, signaltype)
            )

        assert isinstance (timestamp, int) and (timestamp>= 0), ('param `timestamp` '
            'is epoch time and should be a integer >=0. Inputted: %s'%timestamp)
        
        assert tag is not None, "`tag` has to be provided for STOP_ENTRY!"        

        with self.lock:
            """
            Use a lock so that self.message_id is incremented properly.
            """
            try:
                message = locals()
                del(message['self'])   
                message['tag'] = tag[:36] if isinstance(tag, str) else tag
    
                message['strategy_id'] = (strategy_id[:16] if strategy_id is not 
                                              None else self.strategy_id)
                message['message_id'] = self.message_id
                message['ordertype'] = CONSTANTS.ORDERTYPE_BRACKETORDER
                message['signaltype'] = signaltype
                
                # add to messages_today dict
                self.messages_today[self.message_id] = message
                self.socket.send_json(message)
                self.message_id += 1
                return message['message_id']
            except:
                logger.exception(message)
                return 0     

    def meru_exit_bracket_order(self,
            ticker,
            tag,
            strategy_id=None,
            signaltype=CONSTANTS.SIGNALTYPE_EXIT_MARKET,
            timestamp=int(datetime.datetime.now().timestamp() * 1000000),
            ):
        """        
        Method to send an EXIT AT MARKET message to the Meru servers for an 
        existing CONSTANTS.ORDERTYPE_BRACKETORDER order identified by provided
        inputs `tag` and `strategy_id`.
        
        This message/order will be sent to this app running `RabbitmqReconnectingPublisher` 
        class  which  will listen to these messages/orders and in turn forward them 
        to Meru Servers!
        
        
        Parameters
        ----------     
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments
            
            This param is ignored. Provided just for downstream apps.

        tag : str/int of max length 36 chars.
        
        strategy_id : str, of max length 16 chars
            example: 'ALGO1'
            This single app `MeruApp` will handle messages from all your 
            strategies which are live with Meru. In order to map various orders
            to thier right strategies, orders should contain this field 
            `strategy_id`. 
            
            Please agree on `stategy_id` with Meru.
            
            The default is None, which means use the strategy_id attr of this
            `ZMQLocalClient` instance.     
            
        signaltype : CONSTANTS.SIGNALTYPE_EXIT_MARKET
         
            signaltype to be used. It shd match the value of one of three
            specified values.        
            
            If you want to exit the Signal/Position at Current Market price,
            use CONSTANTS.SIGNALTYPE_EXIT_MARKET.
                        
        timestamp : int, optional
            The UTC unix (epoch) timestamp upto microsecond resolution. 
            
            The default is current timestamp. We multiple the timestamp with
            1,000,000 to convert it to an int!
            
            
        Returns
        -------
        int
            message_id:  
                unique `message_id` for this message/order.
                0 if there was an exception in sending message!
        Note1
        -----               
        The `message_id` is not global i.e. it is not shared by all instances
        of `ZMQLocalClient`. Each instance will maintain its own `message_id` and
        thus it is possible that mutliple instances of this class have same 
        `message_id`.
        
        What it means for you is that if you are running just one strategy, it
        can be safely ignored. You have a self incrementing `message_id`.
        
        But if you are running multiple strategies, and each one have an independent
        instance of this class (recommended way!), then each strategy will 
        maintain its own message_id independent of other strategies. So different
        strategies can have same `message_id` at the same time. In such cases
        (`strategy_id`, `message_id`) will represent unique messages sent to
        the Meru Servers.
        
        If you don't intend to use `message_id`, you can ignore it completely.
                
        Note2
        -----
        Some fields are going to be auto-appended to the message when it is
        being sent by the RabbitMQ client like `message_id`, `dealer_id`.
    
        Incidently, (`message_id`, `dealer_id`) are reserved keywords and 
        don't use them in in this class.
            
            `message_id`: A auto incrementing unqiue key for each message
                sent by you. This is internal to and maintained by this 
                'ZMQLocalClient` class and you don't have to do anything.
                
                It will help in reconciliation at both your end and ours.

            `dealer_id` : dealer_id assigned to you by us. It will help us 
                indentify which partner sentX us this message as multiple partners
                might be running their algos with us.                .

        """
        
        assert (signaltype == CONSTANTS.SIGNALTYPE_EXIT_MARKET), (
            'param `signaltype` should be one of CONSTANTS.SIGNALTYPE_EXIT_MARKET". Inputted: %s'%signaltype)    
        
        assert tag is not None, "`tag` has to be provided for EXIT_AT_MARKET signal!"

        with self.lock:
            """
            Use a lock so that self.message_id is incremented properly.
            """
            try:
                message = locals()
                del(message['self'])
                message['tag'] = tag
                message['message_id'] = self.message_id
                message['signaltype'] = signaltype
                message['ticker'] = ticker
                message['strategy_id'] = (strategy_id[:16] if strategy_id is not 
                                              None else self.strategy_id)
                message['ordertype'] = CONSTANTS.ORDERTYPE_BRACKETORDER   
                # add to messages_today dict
                self.messages_today[self.message_id] = message
                self.socket.send_json(message)
                self.message_id += 1
                return message['message_id']
            except:
                logger.exception(message)
                return 0 
                
    def get_tag_for_signal(self, ticker=None, action=None, quantity=None,
            price=None, trigger_price=None, product=None):
        """
        Extract the tag during placing a `NEW_ENTRY` signal for given
        params. If a param is not provided, or None is proved, it will
        be ignored while querying the tag.
        
        This tag is required while placing signal of singaltype 'AMEND_ENTRY'
        or 'STOP_ENTRY'.

        Parameters
        ----------
        ticker : str
            The ticker as per ZEORDHA terminology.
            avialable at  https://api.kite.trade/instruments

        action : {'BUY', 'SELL'}
            'BUY' or 'SELL'            
                    
        quantity : float, optional
            The actual quantity to be bought or sold.
            If it is a derivative ticker, still provide number of shares and 
            not number of lots! For example, for trading two lots on BANKNIFTY 
            futures where each lot is of qty 25, send `quantity` param as 50.
        
            The default is 0, which is provided for only for testing purposes,
            i.e. testing communication between Meru and your app.
            
        price : float, optional
            The `price` param for order. 
            The default is 0.
            
        trigger_price : float, optional
            The `trigger_price` param for order. 
            The default is 0.
            
            if price==0 and trigger_price==0: means MARKET order
            if price>0 and trigger_price==0: means LIMIT order
            if price==0 and trigger_price>0: means SL_M order
            if price>0 and trigger_price>0: means SL order
            
            The order_type is not being sent separately as it can be deduced
            from logic above.
                    
        product : {'MIS','NRML'}, optional
            Product type for the order. 
            The default is `MIS`.

        Returns
        -------
        list of tags which match the filters. Even if only one item is there,
        it will still return a list.
        
        An empty list will be returned in case there is no signal found or
        error is encountered.
        
        Example
        ------
        >>> self.get_tag_for_signal(ticker='ACC', action='BUY', quantity=3)
        Out[10]: ['Hi9G5hkVEeybB2Vm20dsWg']

        """
        try:
            # take last row per tag so that accurate parameter are filtered.
            # if tag is a collection like list, tuple, set, the the step
            # to groupby('tag').last() will fail.
            # Hence it is required that tag is not a list, tuple set etc but 
            # str/int/float. This check is built in method `meru_place_order`.
            df = self.messages_today_as_dataframe.copy().groupby('tag').last()
            # with this tag field has become index now.
            query_string = "(message_id>=0)"
            if ticker is not None:
                query_string = query_string + "&(ticker==@ticker)"
            if action is not None:
                query_string = query_string + "&(action==@action)"
            if quantity is not None:
                query_string = query_string + "&(quantity==@quantity)"
            if price is not None:
                query_string = query_string + "&(price==@price)"
            if trigger_price is not None:
                query_string = query_string + "&(trigger_price==@trigger_price)"
            if product is not None:
                query_string = query_string + "&(product==@product)"
            
            return list(df.query(query_string).index)
        except Exception as e:
            logger.exception(e)
            return []
            
    def stop(self):
        """
        Stop this `local_client`. Run it at EOD only. Even if you don't stop it
        you should be fine.
        
        """
        try:
            self.socket.close()
        except Exception as e:
            logger.exception(e)
        


class RabbitmqReconnectingPublisher(object):
    """
    TO understand the design pattern in rabbitmq, please go through the article
    below:
    
    https://www.cloudamqp.com/blog/2015-05-18-part1-rabbitmq-for-beginners-what-is-rabbitmq.html
    
    Essentially, the publishing app, called publisher (You) will send messages
    (over a queue) to a broker (or Exchange) which is hosted in Meru's server.
    Once a message has been sent to the Exchange, you will receive a delivery
    confirmation for the same.
    
    At the server side, once the message is recieved at exchange, Meru would
    handle the message. Essentially, it will spawn a consumer to which this 
    exchange would deliver this message. So, Essentially:
        
        Producer (you) ---(message)---> Broker (Exchange) At Meru ---(message)---> Consumer at Meru.
    
    Meru is sharing this Producer app with you, which besides publishing
    (sending) messages to Meru's server will handle unexpected interactions and 
    events with RabbitMQ such as channel and connection closures and 
    enusure that publisher app reconnects or comes back online when such events 
    happen. 
        
    Note1
    -----

    1. The default implementations are done using:
        https://github.com/pika/pika/blob/master/examples/asynchronous_publisher_example.py
        https://github.com/pika/pika/blob/master/examples/asynchronous_consumer_example.py
        
        
    Note2: Delivery tags
    --------------------
    
    1. As of now we are not unsing a REQ-REP configuration. We are using a 
        `publisher confirms` protocol. In this protocol, the exchange will
        acknowledge any message received on this channel by sending back a 
        confirmation, even if there is no consumer/handler attached to the 
        exchange to listen to this message. These exchange will confirm these
        messages to this publisher and hold the messages in its memory. When a 
        listner comes online, the exchnage will push all such pending messages 
        at one go. So this `publisher` and `listner` at other end work independently
        and one can work smoothly even if other one is offline.
    
    2. All the messages sent to rabbitmq server will be confirmed by the 
        exchange with ack/nack. Nack will almost never be sent by exchange.
        
    3. A field called `delivery_tag` (which is unique per message per 
        connection)  is sent to the  exchange by this `publisher` while 
        sending any message. This `delivery_tag` is an auto-incrementing integer 
        maintained internally by `pika` library managing the connection and 
        is not exposed externally.
        
    4. While confirming the message, the exchange will send this `delivery_tag`
        back to the publisher, as a part of its method_frame wich contains either
        the method `spec.Basic.Ack` or `spec.Basic.Nack`. This method_frame
        is made available to the channel's `confirm_delivery` callback 
        (i.e. `on_delivery_confirmation` in this publisher)
        
    5. This `delivery_tag` recieved on confirmation can be matched against the
        `delivery_tag` sent with the message and thus a message is fully confimed.
        
    6. If `delivery_tag` is known at the time of sending the message then it
        can be used to track which messages have been received successfully 
        by the server by matching them against the `delivery_tag` received 
        in the confirmation. Thus if a dict of all messages with key as 
        `delivery_tag` is maintained, and if the confirmed messages are knocked
        off from this dict in `on_delivery_confirmation` method, then this dict
        essentially contains the missed messages. On period intervals, these
        missed messages can be sent to server again.
        
    7. The challenge is that `delivery_tag` is not exposed by `pika`, so use
        a dummy attr internally `_delivery_tag` which mimics `delivery_tag`.
            - auto incrementing
            - starts with 1
            
        While sending the messages we use this internal `_delivery_tag` as key
        and while receiving confirmation in `on_delivery_confirmation` method 
        we already have `delivery_tag` as a part of confirmation and use that 
        to pop off the message from the dict.
        
    8. This is slightly complicated by the fact that our reconnecting publisher
        creates a new connection every time theres is connection failure 
        and for each such new connection, pika resets the `delivery_tag` to 1.
        We handle that by reseting `_delivery_tag` to 1 in `run` method which is 
        called everytime a connection is opened. We also maintain another attr
        `_connection_no` which increments by 1 on each new (re)connection.
        this attr combo( `connection_no`, `_delivery_tag`) represent unique
        messages sent by this reconnecting publisher. We use this key to
        map our messages_pending dict.
        
    9.  Another issue is the pika's `Acknowledging Multiple Deliveries at Once`
        feature which is auto-enabled. 
        https://www.rabbitmq.com/confirms.html
        
        So, if multiple messages are sent at a time (say to clear off pending
        messages after a reconnection), and `echange` has got multiple confirmations
        to make it can arbitrarily do one of the three:
            a. Send one confirmation per message
            b. Send confirmations for multiple messages in this lot in one 
                confirmation message by sending 
                    - `delivery_tag` as the  `delivery_tag` of last message to 
                        be confirmed for that lot and multiple=True flag in 
                        method_frame.
            c. Send confirmations for multiple messages in this lot in one 
                confirmation message by sending `delivery_tag` as 0 and 
                multiple=True flag in method_frame. It means all pending 
                messages are confirmed.
                
            When messages are sent in bulk/rapidly, (b) happens frequently
            and has been handled.
              
    10. Everytime a reconnection error, the first thing we do is send all
        pending messages at that point of time.
        
    11. This raises an issue that messages are added to pending messages dict
        when they are sent and poped when their confirmations come. On a day when
        frequent disconnects and reconnects happens, it is possible that 
        same message has to be sent multiple times in different connections
        till it is received by the exchange. Say after 5 attempts it is confimed,
        then only the 6 the entry in our dict is popped. The first 5 entries
        as still there and will be sent again when a reconnection happens.
        
        So we need a different key for this missed messages dict, and a mapper
        which maps (`connection_no`, `_delivery_tag') to this key. When message
        is being sent in all 6 attempts, new entires are added to the mapper
        mapping the (`connection_no`, `_delivery_tag') to this new key, but 
        this new key is unique per message content and thus all of them point
        to same message and there is only one entry in the pending messages dict.
        Once it is confimed on 6th attempt, we get this `key` from the mapper
        and the message is popped from pending messages dict.
        
    12. Each strategy by the partner will have a local instance of `ZMQLocalClient`
        and each will have their local auto-incrementing `message_id`. But this
        publishers serves all of them in one instance. So, we use a tuple  
        (`message_id`, `strategy_id`) as this new key which is unique per message 
        sent by this publisher.
        
    
    13. What is to be noted is that if we add a consumer to the exchange on the 
        other side, it will have its own auto-incrementing `delivery_tag` which 
        has no bearning/relationship with the auto-incrementing `delivery_tag`
        of this publisher. They can have two different numbers dependning upon 
        the number of publisher publishing, the consumers listening, and the 
        umber of reconnections each publisher and consumer have seen.
    
    14. For delivery confirmations, this link is very helpful. It has a 
        google groups link in discussion, which lays the issue with 
        delivery_tags threadbare and sample code to resolve it.
        https://github.com/pika/pika/issues/1212
        
        
        
    AMQP
    ----
    Authoritative and comprehensive AMQP documenatation is available on:
        https://www.rabbitmq.com/amqp-0-9-1-reference.html
        
    rerfer to link to find out more on message properties.
        https://livebook.manning.com/book/rabbitmq-in-depth/chapter-3/101        

    """

    def __init__(self, name=NAME, host=HOST, 
                 port=PORT, exchange=EXCHANGE_NAME, 
                 exchange_type='direct', virtual_host=VIRTUAL_HOST,
                 user=USER, password=PASSWORD, routing_key=ROUTING_KEY,
                 app_id=APP_ID, dealer_id=DEALER_ID, 
                 delivery_confirmations=True):
        """
        Setup the publisher object to connect to RabbitMQ server hosted at 
        Meru's servers.
        
        Parameters
        ----------
        exchange : str, optional
            The name of the exchange. 
            The default is EXCHANGE_NAME defined at top level.

        exchange_type : str, optional
            type of exchange eg. direct, topic etc. Refer Rabbitmq documentation.
            The default is 'direct'.
            
        name : str, optional
            name for this Publisher. It will be used in the name of the thread
            running this publisher. 
            
            The default is NAME defined at top level.

        delivery_confirmations : boolean, optional

            Whether delivery confirmation is enabled or not. If enabled, you 
            will get a confirmation when a message sent by you is recieved by 
            Exchange.
            
            So, if messages are not confirmed withing a certain period (say 5
            seconds), one could assume that message was lost and retry sending.
            However, the Exchange will only send confirmation when certain
            conditions are met, and in very rare cases can take long.

            The default is True.

        Returns
        -------
        None.

        Note
        ----
        1. It will store the basic params for this connection.
        
        2. It will create connection and channel and connect to the exchange 
            when `connect` method is called.
        
        3. It uses default exchange at Meru Server which is always on. A long
            as Meru Server is on, then this client will connect (barring 
            network issues). 
            
           If disconnection happens at Meru server's end, say because either the
           server or the rabbitmq server is rebooted, then it will attempt to 
           reconnect every 5 seconds till the time it gets reconnected again.
            
        """
        credentials = pika.PlainCredentials(USER, PASSWORD)
        self.parameters = pika.ConnectionParameters(
                host=HOST,
                port=PORT,
                virtual_host=VIRTUAL_HOST,
                credentials=credentials,
                connection_attempts=3)         
        
        self._exchange = exchange
        self._exchange_type = exchange_type
        self.app_id = app_id
        self.routing_key = routing_key
        self._delivery_confirmations = delivery_confirmations     
        self._name = self.__class__.__name__ if name is None else name       
        
        
        self._connection = None
        self._connection_no = 0
        """
        int, 
        Initialized at 0. But first connection will have the value of 1 as it
        is incremented before connection is made.
        
        To keep a count of connection number in case of multiple reconnections.
        """
        
        self._channel = None

        self._deliveries = None
        self._acked = None
        self._nacked = None
        # self._delivery_tag = None
        # For each connection delivery_tag is reset to 1 and hence 
        # we have to reset it. The combo of (_delivery_tag, _connection_no)
        # should be unique though.
        self._delivery_tag = None
        self._last_delivered_msg_tag = None



        self._stopping = False
        
        # will be used to seed the `user` property in message.
        self._user = USER
        """
        str
        will be used to seed the `user` `property` in message.
        """
        
        # will be used to see the `type` property in message.
        self._type = 'SIGNAL'
        """
        str
        will be used to set the `type` `property` in message.
        """
        
        self.dealer_id = dealer_id
        """
        str
        
        dealer_id assigned to you by us. This will be auto appended to
        the messages sent by you and allow us to identify which partner
        has sent the signal.
        
        """
        
        self.message_mapper = dict()
        """
        dict.
        
        This reconnecting publisher reconnects by creating a new connection 
        whenever there is a unexpected disconnection. Under the hood, the 
        delivery confirmation mechanism implemented by RabbitMq uses an invisible
        field called `delivery_tag` which is maintained internally by the library.
        
        When a new connection is made, the delivery_tag initializes at 0 and
        then increases by 1 for each message. We maintain `self._delivery_tag`
        attr which exactly equals the internal `delivery_tag` maintained by the
        library and use `self._delivery_tag` for confirming delivery.
        
        When a reconnection happens after an unexpected disconnection, then this
        `delivery_tag` or `self._delivery_tag` is reset to 0. So, 
        `self._delivery_tag` can't be used as a unique key for a message sent.
        
        We will use `message_id` maintained by the `ZMQLocalClient` as unique
        key. Now, the server only returns `delivery_tag` while confirming delivery
        and thus we need a mapper to map a `delivery_tag` to a `message_id`.
        
        key: tuple of (`self_connection_no`, `self._delivery_tag`)
        value : `message_id` which is unique for each message sent.
        
        """
        
        self.messages_pending = dict()
        """
        dict
        
        dict of messages which are not yet confirmed by the Broker to
        this publisher. 
        
        When a message in being published to Broker, it will added to this
        dict and remove as soon as broker sends a delivery confirmation. The
        delivery confirmation is very fast. So, if any message remains in 
        this dict stays beyond 5 seconds or so, then we have a problem. 
        
        key: ('message_id',  'strategy_id') which is `message_id` and 
            `strategy_id` for this publisher. 
        
        value: python dict representing the message.
        
        """
        
        self.message = None
        """dict
        
        Store the last message sent as this attr. Useful for inspecting the
        messages and testing.
        """        
        

    @property
    def is_open(self):
        """
        Find whether RabbimtMQ client is connection or not!

        Returns
        -------
        Boolean
            True if connected else False.
        """
        try:
            return self._connection.is_open
        except:
            return False

    def connect(self):
        """
        This method connects to RabbitMQ, returning the connection handle.
        When the connection is established, the on_connection_open method
        will be invoked by pika.        

        Returns
        -------
        pika.SelectConnection
            pika.SelectConnection connection.

        """

        logger.debug('%s Rabbitmq: connecting..'%(self._name))
        return pika.SelectConnection(
            parameters= self.parameters,
            on_open_callback=self.on_connection_open,
            on_open_error_callback=self.on_connection_open_error,
            on_close_callback=self.on_connection_closed)

    def on_connection_open(self, _unused_connection):
        """
        This method is called by pika once the connection to RabbitMQ has
        been established. It passes the handle to the connection object in
        case we need it, but in this case, we'll just mark it unused.

        Parameters
        ----------
        _unused_connection : pika.SelectConnection 
            pika.SelectConnection object

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Connection opened!'%self._name)
        self.open_channel()

    def on_connection_open_error(self, _unused_connection, err):
        """
        This method is called by pika if the connection to RabbitMQ
        can't be established.

        Parameters
        ----------
        _unused_connection : pika.SelectConnection 
            pika.SelectConnection 
            
            
        err : Exception 
            The Error.

        Returns
        -------
        None.

        """
        logger.exception('%s Rabbitmq connection open failed, '
                     'reopening in 5 seconds: %s'%(self._name, err))
        self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    def on_connection_closed(self, _unused_connection, reason):
        """
        This method is invoked by pika when the connection to RabbitMQ is
        closed unexpectedly. Since it is unexpected, we will reconnect to
        RabbitMQ if it disconnects.

        Parameters
        ----------
        _unused_connection : pika.connection.Connection 
            The closed connection obj.
            
        reason : Exception
            exception representing reason for loss of
            connection.

        Returns
        -------
        None.

        """
        self._channel = None
        if self._stopping:
            self._connection.ioloop.stop()
        else:
            logger.warning('%s Rabbitmq Connection closed, reopening in 5 seconds: %s'%(self._name, reason))
            self._connection.ioloop.call_later(5, self._connection.ioloop.stop)

    def open_channel(self):
        """
        This method will open a new channel with RabbitMQ by issuing the
        Channel.Open RPC command. When RabbitMQ confirms the channel is open
        by sending the Channel.OpenOK RPC reply, the on_channel_open method
        will be invoked.
        
        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Creating a new channel.'%self._name)
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        """
        This method is invoked by pika when the channel has been opened.
        The channel object is passed in so we can make use of it.

        Since the channel is now open, we'll declare the exchange to use.

        Parameters
        ----------
        channel : pika.channel.Channel
            The channel object.

        Returns
        -------
        None.

        """
        logger.info('Rabbitmq Channel connected with Meru Servers for user: %s!'%self._name)
        self._channel = channel
        self.add_on_channel_close_callback()
        if self._delivery_confirmations:
            self._channel.confirm_delivery(self.on_delivery_confirmation)

        # Try to place any messages_pending as well.
        self.republish_pending_messages()
        
        # since this user doesn't have rights to setup up exchange, we
        # will not attempt to set it up.
        # if self._exchange != "":
        #     self.setup_exchange()

    def add_on_channel_close_callback(self):
        """
        This method tells pika to call the on_channel_closed method if
        RabbitMQ unexpectedly closes the channel.

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Adding channel close callback.'%self._name)
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reason):
        """
        Invoked by pika when RabbitMQ unexpectedly closes the channel.
        Channels are usually closed if you attempt to do something that
        violates the protocol, such as re-declare an exchange or queue with
        different parameters. In this case, we'll close the connection
        to shutdown the object.

        Parameters
        ----------
        channel : pika.channel.Channel
            The closed channel.
        reason : Exception
            why the channel was closed

        Returns
        -------
        None.

        """       
        logger.warning('%s Rabbitmq Channel %s was closed: %s'%(self._name, 
                                                        channel, reason))
        self._channel = None
        
        # if the connection closes during market hours because of
        # reboot of kanhoji_server or of rabbitmq-server at kanhoji servers
        # or for any other reasons for any fault at rabbitmq_server's end.
        # reconnect. 
        # after market, let it remain closed! 
        
        # pika.exceptions.ConnectionWrongStateError        
        if not self._stopping:
            try:
                self._connection.close()
            except Exception as e:
                # if happened before market closing, catch the excpetions
                # so that it can reconnect.
                if datetime.datetime.now().time() < CONSTANTS.MARKET_END_TIME:
                    logger.exception(e)                                        
                else:
                    raise                    
                    # logger.exception(e)     
                    
    def setup_exchange(self):
        """
        Setup the exchange on RabbitMQ by invoking the Exchange.Declare RPC
        command. When it is complete, the on_exchange_declareok method will
        be invoked by pika.

        The name of the exchange to declare is passed as `exchange` input at the
        time of initializaion of this object.
        
        It will not be used by this publisher as the user doesn't have sufficient
        user rights.

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Declaring exchange %s'%(self._name, self._exchange))
    
        self._channel.exchange_declare(
            exchange=self._exchange,
            exchange_type=self._exchange_type,
            callback=self.on_exchange_declareok,
            passive=False, durable=False, auto_delete=False
            )

    def on_exchange_declareok(self, _unused_frame):
        """
        Invoked by pika when RabbitMQ has finished the Exchange.Declare RPC
        command.
        
        Parameters
        ----------
        _unused_frame : pika.Frame.Method 
            Exchange.DeclareOk response frame.

        Returns
        -------
        None.

        """

        logger.debug('%s Rabbitmq Exchange declared: %s'%(self._name, 
                                                          self._exchange))
        if self._delivery_confirmations:
            self.enable_delivery_confirmations()
            
    def enable_delivery_confirmations(self):
        """
        Send the Confirm.Select RPC method to RabbitMQ to enable delivery
        confirmations on the channel. The only way to turn this off is to close
        the channel and create a new one.

        When the message is confirmed from RabbitMQ, the on_delivery_confirmation 
        method will be invoked passing in a Basic.Ack or Basic.Nack method from 
        RabbitMQ that will indicate which messages it is confirming or rejecting.

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Issuing Confirm.Select RPC command'%self._name)
        self._channel.confirm_delivery(self.on_delivery_confirmation)

    def on_delivery_confirmation(self, method_frame):
        """
        Invoked by pika when RabbitMQ responds to a Basic.Publish RPC
        command, passing in either a Basic.Ack or Basic.Nack frame with
        the delivery tag of the message that was published. The delivery tag
        is an integer counter indicating the message number that was sent
        on the channel via Basic.Publish. Here we're just doing house keeping
        to keep track of stats and remove message numbers that we expect
        a delivery confirmation of from the list used to keep track of messages
        that are pending confirmation.

        Parameters
        ----------
        method_frame : pika.frame.Method 
            Basic.Ack or Basic.Nack frame.

        Returns
        -------
        None.

        """
        m = method_frame.method
        confirmation_type = m.NAME.split('.')[1].lower()
        multiple = m.multiple
        delivery_tag = m.delivery_tag

        # if all outstanding messages are being confirmed at one go.
        if multiple and delivery_tag == 0:
            delivery_tag = self._deliveries[-1]

        for tag in range(self._last_delivered_msg_tag + 1, delivery_tag + 1):
            if confirmation_type == 'ack':
                message_key = self.message_mapper[(self._connection_no, tag)] 
                message = self.messages_pending.pop(message_key, None)  
                try:
                    logger.info('%s Rabbitmq received %s delivery confirmation on '
                        'connection no: %s, delivery tag: %i, multiple: %s, '
                        'message_key: %s, user_tag: %s!',
                                    self._name,
                                    confirmation_type,
                                    self._connection_no,
                                    delivery_tag,
                                    multiple,
                                    message_key, 
                                    message.get('tag', None)
                        )                                
                except Exception as e:
                    logger.exception("Some issue with delivery tag: {} and "
                                     "message_id {} from strategy_id: {}".format(
                                         str(tag), str(message_key[0]),
                                         str(message_key[1]))
                                     )
                self._acked += 1
                
            elif confirmation_type == 'nack':
                self._nacked_msgs.append(tag)
                self._nacked += 1
                
            self._deliveries.remove(tag)
                    
        self._last_delivered_msg_tag = delivery_tag

        logger.debug(
            '%s Rabbitmq Published %i messages, %i have yet to be confirmed, '
            '%i were acked and %i were nacked', self._name, self._delivery_tag,
            len(self._deliveries), self._acked, self._nacked)

    def publish_message(self, message):
        """
        Publish the message to the Meru's server.

        Parameters
        ----------
        message : Dictionary
            Dict representing a message which you want to send to 
            Meru servers.
            
            This app will add a few fields listed below while sending 
            the messages to Meru. If your existing messages have these 
            fields, they will be overridden here.
            
                `dealer_id`: dealer_id assigned to you by us. It will help us 
                    indentify which partner sent us this message.
                    
        Returns
        -------
        None.

        Note
        ----        
        Pulishing over a new callback gives better performance over applications
        which are launched as multiprocessing processes. Otherwise directly
        publishing leads to a lot of error messages, though the messages get
        published. 
        
        If this implementation turns out to be an issue, just remove everything
        and use the default last line!
                
        """
        cb = functools.partial(self._publish_message, message)
        self._connection.ioloop.add_callback_threadsafe(cb)
#        self._connection.ioloop.call_later(0, cb)
#        self._publish_message(message)
        
    def _publish_message(self, message):
        """
        Publish a message to RabbitMQ.
        
        If _delivery_confirmations is enabled, append a list of deliveries 
        with the message number that was sent. This list will be used to check 
        for delivery confirmations in the on_delivery_confirmations method.

        Parameters
        ----------
        message : dictionary
            Message to be sent to Meru.
            
        Returns
        -------
        None.

        """
        try:

    
            #  user_id property is key authentication mechanism! Meru has
            # provided you are user_id and if you put something else here
            # the message will automatically be rejected by Rabbitmq.
            properties = pika.BasicProperties(
                    app_id=self.app_id,    
                    content_type='application/json',
                    type=self._type,
                    user_id=str(self._user),
                    timestamp=int(datetime.datetime.now().timestamp() * 1000000),
                    message_id=str(self._delivery_tag),
                    )                   
                               
            signaltype = message.get('signaltype')
            tag = message.get('tag')

            if signaltype == CONSTANTS.SIGNALTYPE_EXIT_MARKET:
                # handles both CONSTANTS.ORDERTYPE_SINGLELEGORDER and
                # CONSTANTS.CONSTANTS.ORDERTYPE_BRACKETORDER
                
                assert 'strategy_id' in message, ("field `strategy_id` missing in "
                        "message! it is cumpulsory! Ignoring this message: %s"%message)

                assert 'ticker' in message, ("field `ticker` missing in "
                        "message! it is cumpulsory! Ignoring this message: %s"%message)
                                
                # We add the following fields to the message:
                message['timestamp'] = message.get('timestamp', 
                                    int(datetime.datetime.now().timestamp() * 1000000)
                                    )                            
                # dealer_id will neccessarily be added here, and overridden
                # if provided by you
                message['dealer_id'] = self.dealer_id
                message['app_version'] = CONSTANTS.APP_VERSION            
                message_id = message['message_id']

            elif signaltype == CONSTANTS.SIGNALTYPE_STOP_ENTRY:
                # handles both CONSTANTS.ORDERTYPE_SINGLELEGORDER and
                # CONSTANTS.CONSTANTS.ORDERTYPE_BRACKETORDER
                
                assert 'strategy_id' in message, ("field `strategy_id` missing in "
                        "message! it is cumpulsory! Ignoring this message: %s"%message)

                assert 'ticker' in message, ("field `ticker` missing in "
                        "message! it is cumpulsory! Ignoring this message: %s"%message)
                                
                # We add the following fields to the message:
                message['timestamp'] = message.get('timestamp', 
                                    int(datetime.datetime.now().timestamp() * 1000000)
                                    )                            
                # dealer_id will neccessarily be added here, and overridden
                # if provided by you
                message['dealer_id'] = self.dealer_id
                message['app_version'] = CONSTANTS.APP_VERSION            
                message_id = message['message_id']

                
                
            elif signaltype == CONSTANTS.SIGNALTYPE_NEW_ENTRY:
                # handles both CONSTANTS.ORDERTYPE_SINGLELEGORDER and
                # CONSTANTS.ORDERTYPE_SINGLELEGORDER
                # Additional params to be added for CONSTANTS.ORDERTYPE_BRACKETORDER
                # signaltype == CONSTANTS.SIGNALTYPE_NEW_ENTRY:

                assert 'strategy_id' in message, ("field `strategy_id` missing in "
                        "message! it is cumpulsory! Ignoring this message: %s"%message)
                
                assert 'ticker' in message, ("field `ticker` missing in "
                        "message! it is cumpulsory! Ignoring this message: %s"%message)
                
                assert 'action' in message, ("field `strategy_id` missing in "
                        "message! it is cumpulsory! Ignoring this message: %s"%message)
                
                assert 'quantity' in message, ("field `quantity` missing in "
                        "message! it is cumpulsory! Ignoring this message: %s"%message)
                  
                assert 'message_id' in message, ("field `message_id` missing in "
                        "message! Some error in `ZMQLocalClient` client shipped by "
                        "Meru Captials! Ignoring this message.\n\n**Please contact "
                        "Meru Capitals!**\n\nThe message is: %s"%message)
     
                # We add the following fields to the message:
                message['timestamp'] = message.get('timestamp', 
                                    int(datetime.datetime.now().timestamp() * 1000000)
                                    )
                tag = message.get('tag')
                # allow 36 char tag as well.
                message['tag'] = tag[:36] if isinstance(tag, str) else tag
                message['price'] = message.get('price', 0)
                message['trigger_price'] = message.get('trigger_price', 0)
                message['product'] = message.get('product', CONSTANTS.PRODUCT_MIS)
                            
                # dealer_id will neccessarily be added here, and overridden
                # if provided by you
                message['dealer_id'] = self.dealer_id    
                message['app_version'] = CONSTANTS.APP_VERSION            
                message_id = message['message_id']
            
                # for CONSTANTS.ORDERTYPE_BRACKETORDER, add additional 3 params 
                # for Exit leg.
                if message.get('ordertype') == CONSTANTS.ORDERTYPE_BRACKETORDER:
                    message['stoploss_price'] = message.get('stoploss_price', 0)
                    message['stoploss_trigger_price'] = message.get('stoploss_trigger_price', 0)
                    message['takeprofit_price'] = message.get('takeprofit_price', 0)
                    
            else:
                logger.info("Recieved signaltype: %s! It is not supported and "
                    "ignored! No message sent to Meru Servers!", signaltype)
                return
                            
            # assert dealer_id matches with user_id.
            assert (message['dealer_id'] == properties.user_id), ("`dealer_id` "
                "in message and `user_id` in properties must match! Provided "
                "dealer_id: %s, user_id: %s"%(message['dealer_id'], 
                                              properties.user_id)
                )
            
            message_key = (message_id, message['strategy_id'])
            self.messages_pending[message_key] = message
    
            if self._channel is not None and self._channel.is_open:
                self._channel.basic_publish(
                    exchange=self._exchange, 
                    routing_key=self.routing_key,
                    body=json.dumps(message),
                    properties=properties
                    )
                    
                self.message = message
                
                logger.info("Successufully Published message with message_id: %s, tag: "
                        "%s, message: %s!"%(message_id, tag, message))
            
            
                if self._delivery_confirmations:
                    # map the delivery_tag to message_id of the message
                    self.message_mapper[(self._connection_no, self._delivery_tag)] = message_key
                    self._deliveries.append(self._delivery_tag)
                    self._delivery_tag += 1                    
    
        except:
            logger.exception(message)
            
    def republish_pending_messages(self):
        """
        Re-publish the pending messages again for which delivery confirmations
        haven't been received so far!
        
        When the message is republished it will acquire a new 
        (self._connection_no, self._delivery_tag) combo and a new entry will
        be made in `self.message_mapper` dict. However, on delivery confirmation
        this `self.message_mapper` dict will point to the `message_id` of the 
        message and the message will be popped out from `self.messages_pending`!
        
        We will have many keys in `self.message_mapper` pointing to same
        `message_id` but that should be fine!1

        Returns
        -------
        None.

        """

        for message_id, message in self.messages_pending.items():
            self.publish_message(message=message)
            
    def run(self):
        """
        Connect to exchange and start the IOLoop to publish the messages.
        Also, monitor any disconnections and do any reconnects if required. 
        
        This is blocking in nature.

        Returns
        -------
        None.

        """
        while not self._stopping:
            self._connection = None
            self._deliveries = []
            self._acked = 0
            self._nacked = 0
            # For each connection delivery_tag is reset to 1 and hence 
            # we have to reset it. The combo of (_delivery_tag, _connection_no)
            # should be unique though.
            self._delivery_tag = 1
            self._last_delivered_msg_tag = 0

            self._connection_no += 1

            try:
                self._connection = self.connect()
                self._connection.ioloop.start()
            except KeyboardInterrupt:
                self.stop()
                if (self._connection is not None and
                        not self._connection.is_closed):
                    # Finish closing
                    self._connection.ioloop.start()

        logger.debug('%s Stopped.'%self._name)

    def stop(self):
        """
        Stop the publisher by closing the channel and connection.
        We call the actual stop method in a timer thread to allow any pending
        messages to be published first.

        Returns
        -------
        None.

        """
        logger.debug('%s Rabbitmq Stopping!', self._name)
        self._stopping = True
        self.stop_thread = threading.Timer(interval=5, function=self._stop)
        self.stop_thread.start()
#        self.stop_thread.join(timeout=8)
        
        
    def _stop(self):
        """
        Do the actual work of stopping the publisher by closing the channel and
        then the econnection.

        Returns
        -------
        None.

        """
        self.close_channel()
        self.close_connection()

    def close_channel(self):
        """
        Close the channel with RabbitMQ by sending the Channel.Close RPC command.

        Returns
        -------
        None.

        """
        if self._channel is not None:
            logger.debug('%s Rabbitmq closing the channel', self._name)
            self._channel.close()

    def close_connection(self):
        """
        Close the connection with RabbitMQ.

        Returns
        -------
        None.

        """
        if self._connection is not None:
            logger.info('%s Rabbitmq closing connection', self._name)
            self._connection.close()
            
            
    def run_in_thread(self):
        """
        Connect to the exchange and start the IOLoop to publish messages. 
        Also, monitor any disconnections and do any reconnects in a separate thread. 

        This is non-blocking in nature.
        
        Returns
        -------
        None.

        """
        self.threadname = "MERU.%s.%s.Rabbitmq.Publisher"%(self._exchange, self._name)
        self.thread = threading.Thread(
            target=self.run,
            name=self.threadname,
            daemon=True
            )
        self.thread.start()
            

class MeruApp(object):
    """
    `MeruApp` will sends the final messages to exchange. This is the app which
    you will run.
    
    It will:
        a. have an attr 'rpublisher`: `RabbitmqReconnectingPublisher` object.
            It:
                a. auto restarts and reconnects on rabbitmq originated errors.
                    Reconnects with Meru Servers if there is a error originating
                    from server say server reboot.
                b. connects to Meru server when run 
                c. Does actual sending of messages to Meru's server.
                d. recieves delivery confirmation from Meru server, 
                    when the Meru server receives a message sent by it.
                    
            
        b. zmq: The ZMQ reciever 
        
        c. This app will recieve all messages (orders) sent by you trading 
            applications on `zmq` and then send to Meru server over 
            `rpublisher`.
            
        d. Its `rupublisher` attr can be used to track delivery confirmations\
            pending messages etc.
    """
    
  
    def __init__(self, port_local=PORT_LOCAL,
                 rabbitmq_exchange=EXCHANGE_NAME):     
        """
        Init this `MeruApp`. Most of the parameters are defined at constant
        at top level in this app.

        Parameters
        ----------
        port_local :  int or tuple of ints, optional
            The port to which your trading app will send messages to! It will 
            be listened to by ZMQ.
            
            The default is PORT_LOCAL defined at top level.
            
        rabbitmq_exchange : str, optional
            The name of the exchange. 
            The default is EXCHANGE_NAME defined at top level.
            
        Returns
        -------
        None.

        """
        self.port_local = port_local
        self.is_monitor_local_client = threading.Event()
        self.rabbitmq_exchange = rabbitmq_exchange

        # Create a RabbitmqReconnectingPublisher instance to communicate with
        # Meru's servers. 
        self.create_rpublisher()
       
        # Create a `local_client` listening to messages from main trading 
        # application over ZMQ. 
        self.create_local_client() 

        self.__version__ = CONSTANTS.APP_VERSION


    @property
    def version(self):
        return self.__version__
    
    @property
    def messages_pending(self):
        """
        The messages still waiting to be confirmed, most likely they have
        been not been recieved by Meru Servers!.
        """
        return self.rpublisher.messages_pending

    @property
    def is_rabbitmq_publisher_open(self):
        """
        Find whether its RabbimtMQ publisher is connected or not!

        Returns
        -------
        Boolean
            True if connected else False.
            
        """        
        try:
            return self.rpublisher.is_open
        except:
            return False

    def create_rpublisher(self):
        """
        Create a `RabbitmqReconnectingPublisher` class instance to communicate with
        Meru's servers. 

        Returns
        -------
        None.

        """
        self.rpublisher = RabbitmqReconnectingPublisher(
            exchange=self.rabbitmq_exchange)
        
    def create_local_client(self):
        """
        Initialize the `local_client` listening to messages from your main 
        trading apps (i.e. strategies) over ZMQ and bind this instance to 
        its port!
        
        In case multiple strategies are being pushed, each of them will use a 
        unique port. This single client will listen to messages from all the 
        strategies and bind itself to all the ports being used.

        Returns
        -------
        None.

        """
        try:
            context = zmq.Context()
            self.local_client = context.socket(zmq.SUB)
            if isinstance(self.port_local, tuple):
                for port in self.port_local:
                    logger.info("Binding with port %s", port)
                    self.local_client.connect("tcp://localhost:%s" %port)
            else:
                logger.info("Binding with port %s", self.port_local)
                self.local_client.connect("tcp://localhost:%s" % self.port_local)
                
            # Subscribe to all messsages by passing '' string.
            self.local_client.setsockopt(zmq.SUBSCRIBE, b'')      
            time.sleep(2)
        except Exception as e:
            logger.exception(e)
            
    def start_rpublisher(self):
        """
        Start the Rabbitmq publisher
        """
        self.rpublisher.run_in_thread()
        

    def start_local_client(self):
        """
        Start the `local_client` which will start listening to messages sent by 
        your trading app over ZMQ port(s) defined in `self.port_local`

        Returns
        -------
        None.

        """
        try:
            self.is_monitor_local_client.set()
            self.thread_local = threading.Thread(
                target=self.on_message_local_client,
                name='MERU.ZMQ.Client.Local', 
                daemon=True
                )
            self.thread_local.start()       
            logger.info("Started the Meru ZMQ Client!")            
        except Exception as e:
            logger.exception(e)          
        
            
    def on_message_local_client(self):
        """
        Callback run when a message sent by the main trading application is
        recieved by the `local_client`.
        
        Essentially, it will resend the message to the Meru servers after
        appending a `message_id` and `dealer_id` and little bit of processing.

        Returns
        -------
        None.

        """
        while self.is_monitor_local_client.is_set():
            try:
                message = self.local_client.recv_json()
                self.publish_message_to_meru(message=message)
            except Exception as e:
                logger.exception(e)
            
            
    def publish_message_to_meru(self, message):
        """
        Publish a message to Meru's servers!

        Parameters
        ----------
        message : dict, optional
            The params passed by trading application to sent to Meru in
            method call `meru_place_order` in its `ZMQLocalClient` attr
            are converted into a dict. This dict is passed here as message.
            

        Returns
        -------
        None.

        """
        self.rpublisher.publish_message(message=message)
        
    def stop_rpublisher(self):
        """
        Stop the Rabbitmq publisher. 
        """
        self.rpublisher.stop()
        
    def stop_local_client(self):
        """
        Stop the ZMQ consumer listening to messages from your trading app 
        over local port.

        Returns
        -------
        None.

        """
        try:    
            # clear the flag.
            self.is_monitor_local_client.clear()        
            self.thread_local.join()
        except Exception as e:
            logger.exception(e)            
        
        
    def start_handler(self):
        """
        Start the Rabbitmq publisher as well as ZMQ consumer!

        Returns
        -------
        None.

        """
        try:
            # logger.warning("Starting the MERU ZMQ Client!")
            self.start_rpublisher()
            while not self.is_rabbitmq_publisher_open:
                time.sleep(1)
            # wait for Rpublisher to get connected before starting to recieve
            # messages from local client.
            self.start_local_client()
            time.sleep(2)
        except Exception as e:
            logger.exception(e)      
            
    def stop_handler(self):
        """
        Stop this Rabbitmq Publisher as well as ZMQ consumer.
        
        It will also kill the thread which monitors and reconnects
        this client in case of disconnection.
        
        When this client is interfering with the code, just run this method and
        get rid of ZMQ client!

        Returns
        -------
        None.

        """
        try:    
            # clear the flag.
            self.stop_rpublisher()
            self.stop_local_client()
        except Exception as e:
            logger.exception(e)     
            
    def republish_pending_messages(self):
        """
        Publish the pending messages again for which delivery confirmations
        haven't been received so far!        

        Returns
        -------
        None.

        """
        self.rpublisher.republish_pending_messages()
        pass

        #%%


if __name__ == '__main__':
    import logging
    logger = logging.getLogger('MeruApp')
    logger.setLevel(logging.INFO)
    
    """
    Logging will be done at two places:
        a. Regular Console
        b. To a file names MeruApp_2021-02-02.log or similar, changing as per
            date and stored in $SHOME for your platform.
    """

    formatter = "%(asctime)s.%(msecs)03d | %(module)s | %(levelname)s | %(lineno)d | %(message)s"
    DATE_FORMAT = '%H:%M:%S'    
    formatter_obj = logging.Formatter(fmt=formatter, datefmt=DATE_FORMAT)
    
    # add a file_handler to log to a file.
    date = datetime.datetime.today().strftime("%Y-%m-%d")
    # using ~ as path for log as it can be resolved in all platforms.
    log_path = LOG_PATH or os.path.expanduser(os.path.join("~", "MeruApp_%s.log"%date))
    fh = logging.FileHandler(log_path)
    fh.setFormatter(formatter_obj)
    fh.setLevel(logging.INFO)
    
    # stream_handler ie logging to console.    
    ch = logging.StreamHandler()
    ch.setFormatter(formatter_obj)
    ch.setLevel(logging.INFO)    
    
    logger.addHandler(fh)
    logger.addHandler(ch)      
    
    
    # logging.basicConfig(format=formatter, datefmt=DATE_FORMAT)
    # logger = logging.getLogger(__name__)
    # logger.setLevel(logging.DEBUG)
 
    meru = MeruApp()
    meru.start_handler()
    Z = self = meru.rpublisher
    
    # the pending messages, for which delivery confirmation has not been recieved
    # can be checked:
    meru.rpublisher.messages_pending
    # meru.republish_pending_messages()
    

#    meru.stop_handler()
