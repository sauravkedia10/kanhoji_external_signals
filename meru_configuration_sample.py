#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 10:03:16 2021

@author: saurav
"""
import datetime
import os

# All these values will pe provided by Meru and hardcoded.

# In case the partner is sending signals for multiple strategies, each strategy
# should be bound with its own independent port. If partner is sending signals 
# for two strategies, then provide two ports here. If sending three, then 
# provide three ports. 

# If these ports are specified in config file, the ZMQ subscriber inside the 
# Meru app will automatically subscribe to them.

# PORT_LOCAL = 5556 # for one strategy, just provide the port no. 

# If partner is sending signals for multiple strategies, the same number of 
# ports shd be provided and should be comma separated (i.e tuple) like below.
PORT_LOCAL = 5556, 5557  # sending signals for two stratgeies.
# Put meru's ip in HOST (instead of 127.0.0.1), where rabbitmq is listening 
# at Meru's end.
HOST = '127.0.0.1'
PORT = 5672
# Put meru's rabbitmq's virtual host listening to this routing signal.
VIRTUAL_HOST = ROUTING_KEY  = ''
# Username and password provided to this partner to connect to rabbitmq.
USER = DEALER_ID = APP_ID = NAME = ''
PASSWORD = '' 
# keep EXCHANGE_NAME as blank ('') as we will use default exchange.
EXCHANGE_NAME = ''

date = datetime.datetime.today().strftime("%Y-%m-%d")
LOG_PATH = os.path.expanduser(os.path.join("~", "MeruApp_%s.log"%date))

